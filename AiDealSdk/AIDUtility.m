//
//  AIDUtility.m
//  AiDealSdk
//
//  Created by Appier
//  Copyright (c) 2019 APPIER INC. All rights reserved.
//

#import "AIDUtility.h"
#import <CommonCrypto/CommonDigest.h>
//#import "QGDeviceInfo.h"
#import <UserNotifications/UserNotifications.h>
//#import "QGRequestManager.h"

@interface AIDUtility()

@end

@implementation AIDUtility

static NSString * const SECRET_APPEND = @"supersecretquantumgraph";

static NSString *GroupName;

static AIDUtility *_instance = nil;

+ (instancetype)sharedManager {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _instance = [[self alloc] init];
        /*
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        if ([defaults objectForKey:QG_APP_GROUP_NAME_SDK]) {
            GroupName = [defaults objectForKey:QG_APP_GROUP_NAME_SDK];
        }
        */
    });
    return _instance;
}

//-----------------------------------
#pragma mark - Initialise With APP ID and GROUP
//-----------------------------------
/*
- (void)setAppId:(NSString *)appId withGroupName:(NSString *)group setDevProfile:(BOOL)devProfile {
    GroupName = group;
    [self saveAppGroupName:group withAppId:appId];
    [self saveUserDefaultsWithValue:[NSNumber numberWithBool:devProfile] Key:QG_DEV_PROFILE];
    [self saveUserDefaultsWithValue:appId Key:QG_APP_ID];
    [self saveUserDefaultsWithValue:[self getAppSecret:appId] Key:QG_APP_SECRET];
    if (![self isUserDefaultsForKey:QG_USER_ID]) {
        int64_t userId = [self getUserId];
        if ([self isPreviousUserDefaultsForKey:QG_USER_ID]) {
            userId = [[self getPreviousUserDefaultsForKey:QG_USER_ID] longLongValue];
        }
        [self saveUserDefaultsWithValue:[NSNumber numberWithLongLong:userId] Key:QG_USER_ID];
    }
}

//it is saved in default user defaults not the user defaults with app group
- (void)saveAppGroupName:(NSString *)appGroup withAppId:(NSString *)appId {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:appGroup forKey:QG_APP_GROUP_NAME_SDK];
    [defaults setObject:appId forKey:QG_APP_ID];
    [defaults synchronize];
}
*/
//-----------------------------------
#pragma mark - Profile Info
//-----------------------------------
/*
//returns true if profile info is sent in last 24 hrs
- (BOOL)shouldUserDetailsBeSent
{
    if ([self isUserDefaultsForKey:QG_USER_DETAILS_LAST_SENT_TIME]) {
        NSDate *lastSentTime = [self getUserDefaultsForKey:QG_USER_DETAILS_LAST_SENT_TIME];
        NSTimeInterval secs = [QGCurrentTime() timeIntervalSinceDate:lastSentTime];
        if (secs < 43200) { //send every 12 hours
            return NO;
        }
    }
    return YES;
}

- (NSDictionary *)getUserDetails
{
    //set profile info sent time
    [self saveUserDefaultsWithValue:QGCurrentTime() Key:QG_USER_DETAILS_LAST_SENT_TIME];
    
    NSMutableDictionary *baseDictionary = [[NSMutableDictionary alloc] init];
    [baseDictionary setObject:QGSystemVersion() forKey:@"version"];
    [baseDictionary setObject:QGDeviceModel() forKey:@"model"];
    [baseDictionary setObject:QGModelName() forKey:@"modelName"];
    [baseDictionary setObject:QGIDFV() forKey:@"IDFV"];
    [baseDictionary setObject:QGIDFA() forKey:@"IDFA"];
    [baseDictionary setObject:QGOptOutSet() forKey:@"optOut"];
    [baseDictionary setObject:QG_SDK_VERSION forKey:@"sdkVersion"];
    [baseDictionary setObject:QGAppVersion() forKey:@"appVer"];
    [baseDictionary setObject:QGAppBuildNumber() forKey:@"buildNum"];
    [baseDictionary setObject:QGLanguage() forKey:@"lang"];
    [baseDictionary setObject:QGTimeZone() forKey:@"tz"];
    [baseDictionary setObject:[self getNotificationSettings] forKey:@"nSet"];
    
    NSDictionary *storage = QGDiskSpace();
    if (storage != nil && storage.count) {
        [baseDictionary setObject:storage forKey:@"storage"];
    }
    
    NSString *networkType = QGNetworkType();
    if (networkType.isValidStr) {
        [baseDictionary setObject:networkType forKey:@"networkType"];
        if ([networkType isEqualToString:@"wifi"]) {
            [baseDictionary setObject:[NSNumber numberWithBool:YES] forKey:@"wifi"];
        }
    }
    
    NSDictionary *simInfo = [NSDictionary dictionaryWithDictionary:QGSimCardInfo()];
    if (simInfo.count) {
        [baseDictionary setObject:simInfo forKey:@"carrier"];
    }
    
    if ([self isUserDefaultsForKey:QG_LAST_LOCATION]) {
        [baseDictionary setObject:[self getUserDefaultsForKey:QG_LAST_LOCATION] forKey:@"locn"];
    }
    
    return baseDictionary;
}

- (NSDictionary *)getNotificationSettings {
    NSMutableDictionary *nSet = [[NSMutableDictionary alloc] init];
    if ([QGUtility isRunningTests]) {
        return nSet;
    }
    if (@available(iOS 10.0, *)) {
        //ios 10
        UNUserNotificationCenter * center = [UNUserNotificationCenter currentNotificationCenter];
        [center getNotificationSettingsWithCompletionHandler:^(UNNotificationSettings *settings) {
            [nSet setObject:[self getNotificationValueFor:settings.notificationCenterSetting] forKey:@"ncs"];
            [nSet setObject:[self getNotificationValueFor:settings.lockScreenSetting] forKey:@"lss"];
            [nSet setObject:[self getNotificationValueFor:settings.carPlaySetting] forKey:@"cps"];
            [nSet setObject:[self getNotificationValueFor:settings.soundSetting] forKey:@"ss"];
            [nSet setObject:[self getNotificationValueFor:settings.alertSetting] forKey:@"as"];
            [nSet setObject:[self getNotificationValueFor:settings.badgeSetting] forKey:@"bs"];
            [nSet setObject:[self getNotificationAuthStatus:settings.authorizationStatus] forKey:@"authStatus"];
        }];
    } else {
        //ios 8, 9
        UIUserNotificationSettings *settings = [[UIApplication sharedApplication] currentUserNotificationSettings];
        [nSet setObject:@(settings.types) forKey:@"nt"];
    }
    return nSet;
}

- (NSString *)getNotificationAuthStatus:(UNAuthorizationStatus)status API_AVAILABLE(ios(10.0)){
    if (status == UNAuthorizationStatusDenied) {
        return @"d";
    } else if (status == UNAuthorizationStatusNotDetermined) {
        return @"nd";
    } else if (status == UNAuthorizationStatusAuthorized) {
        return @"a";
    } else {
        return @"Unknown";
    }
}

- (NSString *)getNotificationValueFor:(UNNotificationSetting)setting API_AVAILABLE(ios(10.0)){
    if (setting == UNNotificationSettingNotSupported) {
        return @"ns";
    } else if (setting == UNNotificationSettingDisabled) {
        return @"d";
    } else if (setting == UNNotificationSettingEnabled) {
        return @"e";
    } else {
        return @"Unknown";
    }
}
*/
//-----------------------------------
#pragma mark - User ID & SHA String
//-----------------------------------

/* = 64 bit number if architecture is 64 bit
 rand2 (32 bit) if architecture is 32 bit */
/*
- (int64_t)getUserId {
    int64_t rand1 = (int64_t)(arc4random() % LONG_LONG_MAX);
    int64_t rand2 = (int64_t)(arc4random() % LONG_LONG_MAX);
    return llabs((rand1 << 32) | rand2);
}

- (NSString *)getAppSecret:(NSString *)appId {
    return [self SHAstring:[NSString stringWithFormat:@"%@%@", appId, SECRET_APPEND]];
}

- (NSString *)SHAstring:(NSString *)appId {
    NSData *data = [appId dataUsingEncoding:NSUTF8StringEncoding];
    uint8_t digest[CC_SHA1_DIGEST_LENGTH];
    CC_SHA1(data.bytes, (CC_LONG)data.length, digest);
    
    NSMutableString *output = [NSMutableString stringWithCapacity:CC_SHA1_DIGEST_LENGTH * 2];
    for (int i = 0; i < CC_SHA1_DIGEST_LENGTH; i++)
    {
        [output appendFormat:@"%02x", digest[i]];
    }
    
    return output;
}

//-----------------------------------
#pragma mark - Date Calculation Method
//-----------------------------------
- (NSInteger)daysBetweenDate:(NSDate *)fromDateTime andDate:(NSDate *)toDateTime {
    NSDate *fromDate;
    NSDate *toDate;

    NSCalendar *calendar = [NSCalendar currentCalendar];

    [calendar rangeOfUnit:NSCalendarUnitDay startDate:&fromDate interval:NULL forDate:fromDateTime];
    [calendar rangeOfUnit:NSCalendarUnitDay startDate:&toDate interval:NULL forDate:toDateTime];

    NSDateComponents *difference = [calendar components:NSCalendarUnitDay fromDate:fromDate toDate:toDate options:0];

    return [difference day];
}

//-----------------------------------
#pragma mark - Click Through & View Through
//-----------------------------------

//click through attribution for inApp and notificaiton click
- (void)setClickThroughForNotificationId:(NSNumber *)notificationId {
    [self saveUserDefaultsWithValue:notificationId Key:QG_LAST_CLICKED_NOTIFICATION_ID];
    [self saveUserDefaultsWithValue:QGCurrentTime() Key:QG_LAST_CLICKED_NOTIFICATION_TIME];
}

- (NSInteger)getClickAttributionWindow {
    if ([self isUserDefaultsForKey:QG_CLICK_ATTRIBUTION_WINDOW]) {
        return [[self getUserDefaultsForKey:QG_CLICK_ATTRIBUTION_WINDOW] integerValue];
    }
    return 86400;   //default to 24hrs(in second)
}

//check if last clicked notif. time is less than 24 hrs
- (BOOL)isClickThroughAttribution {
    if ([self isUserDefaultsForKey:QG_LAST_CLICKED_NOTIFICATION_TIME]) {
        NSDate *lastClickedNotificationTime = [self getUserDefaultsForKey:QG_LAST_CLICKED_NOTIFICATION_TIME];
        NSTimeInterval secs = [QGCurrentTime() timeIntervalSinceDate:lastClickedNotificationTime];
        if (secs <= [self getClickAttributionWindow]) {
            return YES;
        }else {
            [self removeUserDefaultsForKey:QG_LAST_CLICKED_NOTIFICATION_TIME];
            return NO;
        }
    }
    return NO;
}

- (NSInteger)getViewThroughAttributionWindow {
    if ([self isUserDefaultsForKey:QG_VIEW_THROUGH_ATTRIBUTION_WINDOW]) {
        return [[self getUserDefaultsForKey:QG_VIEW_THROUGH_ATTRIBUTION_WINDOW] integerValue];
    }
    return 3600;   //default to 1hr(in second)
}

//view through attribution for inApp
- (void)setViewThroughForNotificationId:(NSNumber *)notificationId {
    [self saveUserDefaultsWithValue:notificationId Key:QG_LAST_VIEW_THROUGH_NOTIFICATION_ID];
    [self saveUserDefaultsWithValue:QGCurrentTime() Key:QG_LAST_VIEW_THROUGH_TIME];
}

- (BOOL)isViewThroughAttribution {
    if ([self isUserDefaultsForKey:QG_LAST_VIEW_THROUGH_TIME]) {
        NSDate *viewThroughTime = [self getUserDefaultsForKey:QG_LAST_VIEW_THROUGH_TIME];
        NSTimeInterval secs = [QGCurrentTime() timeIntervalSinceDate:viewThroughTime];
        if (secs <= [self getViewThroughAttributionWindow]) {
            return YES;
        }else {
            [self removeUserDefaultsForKey:QG_LAST_VIEW_THROUGH_TIME];
            return NO;
        }
    }
    return NO;
}
*/
//-----------------------------------
#pragma mark - UserDefaults Methods
//-----------------------------------

+ (NSUserDefaults *)userDefaults {
    return [[NSUserDefaults alloc] initWithSuiteName:GroupName];
}

//save userDefaults with a key and value
- (void)saveUserDefaultsWithValue:(id)value Key:(NSString*)key {
    NSUserDefaults *defaults = [AIDUtility userDefaults];
    [defaults setObject:value forKey:key];
    [defaults synchronize];
}

- (void)saveUserDefaultsWithInteger:(NSInteger)value forKey:(NSString*)key {
    NSUserDefaults *defaults = [AIDUtility userDefaults];
    [defaults setInteger:value forKey:key];
    [defaults synchronize];
}

- (id)getUserDefaultsForKey:(NSString *)key {
    return [[AIDUtility userDefaults] objectForKey:key];
}

- (NSInteger)getUserDefaultIntegerForKey:(NSString *)key {
    return [[AIDUtility userDefaults] integerForKey:key];
}

//check for a key in the userDefaults
- (BOOL)isUserDefaultsForKey:(NSString *)key {
    return ([[AIDUtility userDefaults] objectForKey:key] != nil) ? YES : NO;
}

- (void)removeUserDefaultsForKey:(NSString *)key {
    NSUserDefaults *defaults = [AIDUtility userDefaults];
    [defaults removeObjectForKey:key];
    [defaults synchronize];
}

//NSUserDefaults StandardUserDefaults used before widget
- (BOOL)isPreviousUserDefaultsForKey:(NSString *)key {
    return ([[NSUserDefaults standardUserDefaults] objectForKey:key] != nil) ? YES : NO;
}

- (id)getPreviousUserDefaultsForKey:(NSString *)key {
    return [[NSUserDefaults standardUserDefaults] objectForKey:key];
}

- (void)saveEncodedData:(id)encodableObject forKey:(NSString *)key {
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:encodableObject];
    [self saveUserDefaultsWithValue:data Key:key];
}

- (id)getDecodedDataForKey:(NSString*)key {
    NSData *data = [self getUserDefaultsForKey:key];
    id obj = nil;
    if (data) {
        @try {
            obj = [NSKeyedUnarchiver unarchiveObjectWithData:data];
            return obj;
        }
        @catch (NSException *exception) {
            return obj;
        }
    }
    return obj;
}

//-----------------------------------
#pragma mark - Helpers Class Methods
//-----------------------------------

+ (BOOL)isValidObject:(id)object {
    BOOL valid = YES;
    
    if (!object || (object == nil) || ([object isKindOfClass:[NSNull class]])) {
        valid = NO;
    }
    return valid;
}

NSDate * AIDCurrentTime() {
    return [NSDate date];
}
/*
+ (void)assertPropertyTypes:(NSDictionary *)properties {
    for (id __unused k in properties) {
        NSAssert([k isKindOfClass: [NSString class]], @"keys must be NSString. got: %@ %@", [k class], k);
        
        NSAssert([properties[k] isKindOfClass:[NSString class]] ||
                 [properties[k] isKindOfClass:[NSNumber class]] ||
                 [properties[k] isKindOfClass:[NSNull class]] ||
                 [properties[k] isKindOfClass:[NSArray class]] ||
                 [properties[k] isKindOfClass:[NSDictionary class]] ||
                 [properties[k] isKindOfClass:[NSDate class]] ||
                 [properties[k] isKindOfClass:[NSURL class]],
                 @"property values must be NSString, NSNumber, NSNull, NSArray, NSDictionary, NSDate or NSURL. got: %@ %@", [properties[k] class], properties[k]);
    }
}

+ (BOOL)inBackground {
    return [UIApplication sharedApplication].applicationState == UIApplicationStateBackground;
}

+ (BOOL)isRichPushSupported {
    return QGRichPushSupported();
}

//-----------------------------------
#pragma mark - Show Push Prompt
//-----------------------------------
//fetch show push prompt data on every app launch
- (void)fetchShowPushPrompt {
    
    [QGRequestManager requestShowPushPrompt:^(NSData * _Nullable data, NSURLResponse * _Nonnull response, NSError * _Nonnull error) {
        if (error || ![QGUtility isValidObject:data]) {
            return;
        }
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
        if ([httpResponse statusCode] == 200) {
            //----get data and save to user defaults----
            NSError *jsonError = nil;
            NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:data options:0 error:&jsonError];
            if (jsonError) {
                return;
            }
            QGUtility *util = [QGUtility sharedManager];
            if ([QGUtility isValidObject:dictionary] && [QGUtility isValidObject:dictionary[@"push-prompt"]]) {
                [util saveUserDefaultsWithValue:dictionary[@"push-prompt"] Key:QG_SHOW_PUSH_PROMPT];
            } else {
                [util saveUserDefaultsWithValue:@true Key:QG_SHOW_PUSH_PROMPT];
            }
        }
    }];
}

- (BOOL)isValidDeepLink:(NSURL *)url {
    
    BOOL isSelfUniversalLink = false;
    NSArray<NSString *> *domains = [self getUserDefaultsForKey:QG_UNIVERSAL_LINK_DOMAINS];
    if (@available(iOS 9.0, *)) {
        //Important: iOS will only attempt to fetch the AASA (apple-app-site-association) file over a secure connection (HTTPS).
        if (domains != nil && [[url scheme] isEqualToString:@"https"] && [domains containsObject:[url host]]) {
            isSelfUniversalLink = true;
        }
    }
    return isSelfUniversalLink;
}

- (void)handleDeepLink:(NSURL *)url {
  
    if ([self isValidDeepLink:url]) {
        //
        //   if universal link is set and continueUserActivity is not defined
        //   in AppDelegate, application is likely to crash
        //
        @try {
            dispatch_async(dispatch_get_main_queue(), ^{
                NSUserActivity* userActivity = [[NSUserActivity alloc] initWithActivityType:NSUserActivityTypeBrowsingWeb];
                userActivity.webpageURL = url;
                [[UIApplication sharedApplication].delegate application:[UIApplication sharedApplication]
                                                   continueUserActivity:userActivity
                                                     restorationHandler:^(NSArray *restorableObjects) {}];
            });
        }
        @catch (NSException *exception) {
            NSLog(@"QGSdk: %@ exception calling continueUserActivity: %@", self, exception);
        }
    } else {
        dispatch_async(dispatch_get_main_queue(), ^{
            if (@available(iOS 10.0, *)) {
                [[UIApplication sharedApplication] openURL:url options:@{} completionHandler:^(BOOL success) {}];
            } else {
                [[UIApplication sharedApplication] openURL:url];
            }
        });
    }
}

#pragma mark - Inside Unit Test

static NSNumber *__isRunningTests = nil;

+ (BOOL)isRunningTests {
    if (__isRunningTests == nil) {
        NSDictionary *environment = [[NSProcessInfo processInfo] environment];
        NSString *isRunningTestsValue = environment[@"QG_UNIT_TEST"];
        __isRunningTests = @([isRunningTestsValue isEqualToString:@"YES"]);
    }
    
    return [__isRunningTests boolValue];
}

#pragma mark - Crash Log
+ (NSArray<NSString *> *)crashLogConcernKeywords {
   
    return @[@"QGSdk", @"QGraph", @"QGUtility", @"QGDeviceInfo", @"QGRequestManager", @"QGInAppManager", @"QGInboxManager",
                @"QGImageManager", @"QGLocationManager", @"QGLocalNotificationManager", @"QGWKWebView", @"AIQP",
                @"AIQP+ReactNative", @"AIQP+DataFetch", @"QGNotificationSDK", @"QGGZIP", @"QGStringUtil", @"QGUncaughtExceptionHandler", @"QGInApp", @"QGInbox", @"QGInAppWebViewController", @"QGInAppDisplayManager"];
}
*/
@end
