//
//  AIDWebViewController.h
//
//  Created by Appier on 19/2/19.
//  Copyright © 2019 Appier. All rights reserved.
//

#import <UIKit/UIKit.h>

@class AIDWebViewController;
NS_ASSUME_NONNULL_BEGIN
@protocol AIDWebViewControllerDelegate <NSObject>
- (void)aidWebViewDismissWithPermanentClose:(BOOL)permanentClose WebView:(AIDWebViewController *)inAppWebView;
- (void)aidWebViewCopiedCoupon;
@end

@interface AIDWebViewController : UIViewController
- (instancetype)initWithHTMLString:(NSString *)htmlString CouponCode:(NSString *)couponCode;

@property (nonatomic, weak) id<AIDWebViewControllerDelegate> delegate;
@end

NS_ASSUME_NONNULL_END
