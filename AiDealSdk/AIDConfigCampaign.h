//
//  AIDConfig.h
//
//  Created by Appier on 2020/2/4.
//  Copyright © 2020 Appier. All rights reserved.
//

#import <Foundation/Foundation.h>
NS_ASSUME_NONNULL_BEGIN
typedef enum AIDConfigLANG : NSUInteger {
    AIDConfigLANG_Universal = 0,
    AIDConfigLANG_EN,
    AIDConfigLANG_JP
} AIDConfigLANG;

@interface AIDConfig : NSObject <NSCoding>
@property (nonatomic, copy) NSNumber *campaignId;
@property (nonatomic, copy) NSNumber *elementId;
@property (nonatomic, copy) NSString *badgeUrl;
@property (nonatomic, copy) NSString *badgeText;
@property (nonatomic, copy) NSString *lang;
@property (nonatomic, copy) NSString *limit;
@property (nonatomic, copy) NSString *badgeBaseColor;
@property (nonatomic, copy) NSNumber *countDownStartTimestamp;
@property (nonatomic, copy) NSNumber *campaignControlPercent;
@property (nonatomic, copy) NSNumber *enableTimer;
@property (nonatomic, assign) BOOL isControl; // If YES, let the state disable

- (id)initWithData:(NSDictionary *) campaign;
+ (int)totoalCountDownPeriodFromConfig:(AIDConfig *)config;
+ (AIDConfigLANG)checkLangType:(NSString *)lang;
@end

@interface AIDCampaign : NSObject
@property (nonatomic, copy) NSNumber *elementId;
@property (nonatomic, copy) NSString *css;
@property (nonatomic, copy) NSString *offer;
@property (nonatomic, copy) NSString *present;

- (id)initWithData:(NSDictionary *) campaign;
@end

NS_ASSUME_NONNULL_END
