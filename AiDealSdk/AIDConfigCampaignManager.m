//
//  AIDConfigCampaignManager.m
//
//  Created by Appier on 2020/2/4.
//  Copyright © 2020 Appier. All rights reserved.
//

#import "AIDConfigCampaignManager.h"
#import "AIDRequestManager.h"
#import "AIDImageManager.h"
#import "AIDUtility.h"

//AIDEAL
#define AID_CONFIG           @"aid_config"
#define AID_CAMPAIGN         @"aid_campaign"
#define AID_RANDOM_VALUE     @"aid_random_value"
#if NEW_STAGING
#define CONFIG_CAMPAIGN_URL_FORMAT @"https://visitor.zenclerk-dev.com/api/v1/%@/config.json"
#define PRESENT_CAMPAIGN_URL_FORMAT @"https://visitor.zenclerk-dev.com/api/v1/%@/campaign.json"
#else
#define CONFIG_CAMPAIGN_URL_FORMAT @"https://f1.zenclerk.com/api/v1/%@/config.json"
#define PRESENT_CAMPAIGN_URL_FORMAT @"https://f1.zenclerk.com/api/v1/%@/campaign.json"
#endif
@interface AIDConfigCampaignManager()
@end
@implementation AIDConfigCampaignManager

static int kRandomControlValue = 0;

///----------------------------------
#pragma mark - initialise default values
///----------------------------------
+ (instancetype)instance {
    
    static AIDConfigCampaignManager *instance_ = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance_ = [[self alloc] init];
        
        // Random int to calculate is_control
        if ([[AIDUtility sharedManager] getUserDefaultsForKey:AID_RANDOM_VALUE] == nil) {
            kRandomControlValue = arc4random() % 100;
            NSLog(@"Random Number:%d", kRandomControlValue);
            [[AIDUtility sharedManager] saveUserDefaultsWithValue:[NSNumber numberWithInt:kRandomControlValue] Key:AID_RANDOM_VALUE];
        } else {
            kRandomControlValue = [[[AIDUtility sharedManager] getUserDefaultsForKey:AID_RANDOM_VALUE] intValue];
            NSLog(@"The Existing Random Number:%d", kRandomControlValue);
        }
    });
    return instance_;
}

///----------------------------------
#pragma mark - Server calls - fetch campaigns
///----------------------------------
//----get an updated list of in app campaigns----
- (void)fetchAiDealConfigAndCampaignsWithApiKey:(NSString *)apiKey CompletionHandler:(void (^)(bool success))completionHandler {
    
    __block bool successConfig = NO;
    __block bool successCampaign = NO;
    dispatch_group_t group = dispatch_group_create();
    dispatch_group_enter(group);
    [self requestAiDealConfigWithApiKey:apiKey
                                           CompletionHandler:^(NSData * _Nullable data, NSURLResponse * _Nonnull response, NSError * _Nullable error) {
         if (!error) {
             NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
             if ([httpResponse statusCode] == 200) {
                 //----get data and save to user defaults----
                 [self parseConfigResponse:data];
             }
             successConfig = ([httpResponse statusCode] == 200);
         }
         dispatch_group_leave(group);
     }];
    
    dispatch_group_enter(group);
    [self requestAiDealCampaignsWithApiKey:apiKey
                                          CompletionHandler:^(NSData * _Nullable data, NSURLResponse * _Nonnull response, NSError * _Nullable error) {
        if (!error) {
            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
            if ([httpResponse statusCode] == 200) {
                //----get data and save to user defaults----
                [self parseCampaingsResponse:data];
            }
            successCampaign = ([httpResponse statusCode] == 200);
        }
        dispatch_group_leave(group);
    }];
    dispatch_group_notify(group, dispatch_get_main_queue(), ^{
        
        bool success = successConfig && successCampaign;
        if (success == NO) {
            //Clean all cache if one of requests fail
            [[AIDUtility sharedManager] removeUserDefaultsForKey:AID_CONFIG];
            [[AIDUtility sharedManager] removeUserDefaultsForKey:AID_CAMPAIGN];
        }
        completionHandler(success);
    });
}

- (void)requestAiDealConfigWithApiKey:(NSString *)apiKey
                            CompletionHandler:(void (^)(NSData * _Nullable data, NSURLResponse *response, NSError * _Nullable error))completionHandler {
    NSString *urlString = [NSString stringWithFormat:CONFIG_CAMPAIGN_URL_FORMAT, apiKey];
    NSLog(@"url:%@", urlString);
    NSURL *url = [NSURL URLWithString:[urlString stringByRemovingPercentEncoding]];
    NSURLRequest *request = [NSURLRequest requestWithURL:url cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:60];
    NSURLSessionConfiguration *conf = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:conf];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        completionHandler(data, response, error);
    }];
    [task resume];
}

- (void)requestAiDealCampaignsWithApiKey:(NSString *)apiKey
                            CompletionHandler:(void (^)(NSData * _Nullable data, NSURLResponse *response, NSError * _Nullable error))completionHandler {
    NSString *urlString = [NSString stringWithFormat:PRESENT_CAMPAIGN_URL_FORMAT, apiKey];
    NSLog(@"url:%@", urlString);
    NSURL *url = [NSURL URLWithString:[urlString stringByRemovingPercentEncoding]];
    NSURLRequest *request = [NSURLRequest requestWithURL:url cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:60];
    NSURLSessionConfiguration *conf = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:conf];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        completionHandler(data, response, error);
    }];
    [task resume];
}

- (void)parseConfigResponse:(NSData *)data {

    NSError *jsonError = nil;
    NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:0 error:&jsonError];
    if (!jsonError && dict) {
        [self saveConfigResponse:dict];
    }
}

- (void)parseCampaingsResponse:(NSData *)data {
    
    NSError *jsonError = nil;
    NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:0 error:&jsonError];
    if (!jsonError && dict) {
        [self saveCampaignResponse:dict];
    }
}

// true -- is control group, so we dont show badge and creative
- (unsigned int)xorShift32Generator:(int)input {
    input ^= (unsigned int)(input << 13);
    input = (unsigned int)input;
    input ^= (unsigned int)(input >> 17);
    input = (unsigned int)input;
    input ^= (unsigned int)(input << 5);
    input = (unsigned int)input;
    return input;
}

- (bool)isCampaignControlCampaignId:(NSInteger)campaignId CampaignControlPercent:(int)controlPercent AssignedByApp:(int)appRdNum {

    //gen
    long gen = [self xorShift32Generator:(int)campaignId];

    //p
    NSMutableArray *p = [NSMutableArray new];
    
    //a
    NSMutableArray *a = [NSMutableArray new];
    for (int i = 0; i < 100; i++) {
        [a addObject:[NSNumber numberWithInt:i]];
    }
    
    for (int i = 0; i < controlPercent; i++) {
        //NSLog(@"start:%ld", gen);
        int index = (int)(gen % a.count);
        //NSLog(@"index:%d %d", index, [[a objectAtIndex:index] intValue]);
        [p addObject:[a objectAtIndex:index]];
        [a removeObjectAtIndex:index];
        
        gen = [self xorShift32Generator:(int)gen];
        //NSLog(@"%ld", gen);
    }
    //NSLog(@"p:%@", p);
    //NSLog(@"a:%@", a);
    NSInteger index = [p indexOfObject:[NSNumber numberWithInt:appRdNum]];
    if (index == NSNotFound) {
        return false;
    }
    return true;
}

//----saves all campaigns which have been downloaded----
- (void)saveConfigResponse:(NSDictionary *)dict {
    
    const NSString *outerKey = @"campaignObjectsByElementId";
    if ([dict objectForKey:outerKey]) {
        
        NSMutableDictionary *savedCampaigns = nil;
        if ([[AIDUtility sharedManager] isUserDefaultsForKey:AID_CONFIG]) {
            savedCampaigns = [[AIDUtility sharedManager] getDecodedDataForKey:AID_CONFIG];
        }
        if (savedCampaigns == nil) {
            savedCampaigns = [NSMutableDictionary new];
        }
                
        // New campaign list
        NSDictionary *campaignDict = [dict objectForKey:outerKey];
        [campaignDict enumerateKeysAndObjectsUsingBlock:^(id  _Nonnull key, id  _Nonnull obj, BOOL * _Nonnull stop) {
              //---while initialising config, image is also downloaded simultaneously---
              AIDConfig *config = [[AIDConfig alloc] initWithData:obj];
            
              //---is control ---
              config.isControl = [self isCampaignControlCampaignId:config.campaignId.integerValue CampaignControlPercent:config.campaignControlPercent.intValue AssignedByApp:kRandomControlValue];
            
              //---if there is an empty or nil campaign---
              if (![AIDUtility isValidObject:config]) return;
                          
              //----retain the countDownStartTimestamp for existing campaigns----
              if ([savedCampaigns objectForKey:config.elementId]) {
                  AIDConfig *savedConfig = [savedCampaigns objectForKey:config.elementId];
                  config.countDownStartTimestamp = savedConfig.countDownStartTimestamp;
                  
                  //Clean timestamp to make sure we don't remain previous timestamp
                  //day01 enableTimer(0): 2019/09/09 17:20:20 t:2019 09/09 17:20:20
                  //day02 enableTimer(x): 2019/09/10 18:00:00 t:2019 09/09 17:20:20
                  //day03 enableTimer(0): 2019/09/11 09:00:01 t:2019 09/09 17:20:20 count-down is immediately over
                  if (config.enableTimer.boolValue == NO) {
                      config.countDownStartTimestamp = [NSNumber numberWithLongLong:0];
                  }
              }
              [savedCampaigns setObject:config forKey:config.elementId];
        }];
        
        // We may delete any campaign that are not found in new campaign dict
        NSMutableArray *keysRemoval = [NSMutableArray new];
        [savedCampaigns enumerateKeysAndObjectsUsingBlock:^(id  _Nonnull key, id  _Nonnull obj, BOOL * _Nonnull stop) {
            AIDConfig *config = obj;
            if ([campaignDict objectForKey:config.elementId.stringValue] == NO) {
                [keysRemoval addObject:key];
            }
        }];
        if (keysRemoval.count) {
            [savedCampaigns removeObjectsForKeys:keysRemoval];
        }
        
        NSLog(@"We got %d campaigns ~~ ", (int)savedCampaigns.count);
        [[AIDUtility sharedManager] saveEncodedData:savedCampaigns forKey:AID_CONFIG];
    }
}

- (void)saveCampaignResponse:(NSDictionary *)dict {

    if (![AIDUtility isValidObject:dict]) {
        return;
    }
    //Always use latest campaigns
    NSMutableDictionary *campaigns = [NSMutableDictionary new];
    [dict enumerateKeysAndObjectsUsingBlock:^(id  _Nonnull key, id  _Nonnull obj, BOOL * _Nonnull stop) {
                    
            //---while initialising campaign, image is also downloaded simultaneously---
            AIDCampaign *campaign = [[AIDCampaign alloc] initWithData:obj];
            
            //---if there is an empty or nil campaign---
            if (![AIDUtility isValidObject:campaign]) return;
                
            //Assign elementId wit value of key, ex: 3050, 17709
            campaign.elementId = [NSNumber numberWithLongLong:[key longLongValue]];
        
            [campaigns setObject:campaign forKey:campaign.elementId];
    }];
    if (campaigns.count) {
        [[AIDUtility sharedManager] saveEncodedData:campaigns forKey:AID_CAMPAIGN];
    }
}

- (AIDConfig *)getConfigByElementId:(NSNumber *)elementId {
    
    if (elementId && [[AIDUtility sharedManager] isUserDefaultsForKey:AID_CONFIG]) {
        
        NSMutableDictionary *savedConfigs = [[AIDUtility sharedManager] getDecodedDataForKey:AID_CONFIG];
        AIDConfig *config = [savedConfigs objectForKey:elementId];
        return config;
    }
    return nil;
}

- (AIDCampaign *)getCampaignByElementId:(NSNumber *)elementId {
    
    if (elementId && [[AIDUtility sharedManager] isUserDefaultsForKey:AID_CAMPAIGN]) {
        
        NSMutableDictionary *savedCampaigns = [[AIDUtility sharedManager] getDecodedDataForKey:AID_CAMPAIGN];
        AIDCampaign *campaign = [savedCampaigns objectForKey:elementId];
        return campaign;
    }
    return nil;
}

- (void)updateConfig:(AIDConfig *)configForUpdate {
    
    NSMutableDictionary* savedCampaigns = [[AIDUtility sharedManager] getDecodedDataForKey:AID_CONFIG];
    if (savedCampaigns && savedCampaigns.count && configForUpdate.elementId) {
        
        [savedCampaigns setObject:configForUpdate forKey:configForUpdate.elementId];
        [[AIDUtility sharedManager] saveEncodedData:savedCampaigns forKey:AID_CONFIG];
    }
}

- (void)resetCountDownOfAllCampaigns {
    
    NSMutableDictionary* savedCampaigns = [[AIDUtility sharedManager] getDecodedDataForKey:AID_CONFIG];
    if (savedCampaigns && savedCampaigns.count) {
        
        [savedCampaigns enumerateKeysAndObjectsUsingBlock:^(id  _Nonnull key, id  _Nonnull obj, BOOL * _Nonnull stop) {

                AIDConfig *config = (AIDConfig *)obj;
                config.countDownStartTimestamp = [NSNumber numberWithLongLong:0];
                [savedCampaigns setObject:config forKey:config.elementId];
        }];
        [[AIDUtility sharedManager] saveEncodedData:savedCampaigns forKey:AID_CONFIG];
    }
}
@end

@implementation AIDEvent
+ (NSString *)Eventname_SaveCampaign {
    return @"save_campaign";
}

+ (NSString *)Eventname_UserInput {
    return @"user_behavior";
}

+ (NSString *)Eventname_Conversion {
    return @"conversion";
}
@end
