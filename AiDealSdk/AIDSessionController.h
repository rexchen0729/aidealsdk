//
//  AIDSession.h
//
//  Created by Appier on 2020/2/17.
//  Copyright © 2020 Appier. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AIDSessionInfo.h"

@protocol AIDSessionControllerDelegate <NSObject>
- (void)sessionResumeFromBackground;
@end

NS_ASSUME_NONNULL_BEGIN

@interface AIDSessionController : NSObject
- (instancetype)init;

- (BOOL)isSessionTimeout;

- (void)cleanSessionInfo;

- (void)cleanSessionInfoStorage;

- (void)cleanPageInfoHistory; //Ex: clean SID

- (void)extendSession;

- (BOOL)isUserInputSuppressed;

- (void)reportWhenSocketDisconnected;

- (nullable AIDSessionInfo *)getStoredSessionInfo;

//SessionInfo - UUID USID SID upon init complete
- (void)storePageInfo:(NSDictionary *)sessionInfo;

- (nullable NSDictionary *)getPageInfoAsDictionary;

- (BOOL)isPageInfoReady;

//Campaign event - userInputSurpressed
- (void)storeReceivedCampaignEvent:(NSDictionary *)campaignEvent;

- (nullable AIDReceiveCampaignEvent *)getStoredReceivedCampaginEvent;

- (nullable NSDictionary *)getStoredReceivedCampaginEventForEvent;

- (void)removeStoredReceivedCampaginEvent;

- (nullable NSNumber *)getStoredElementId; // A convenient method to get elementId

//User states
- (nullable AIDSessionUserState *)getStoredSessionUserState;

- (void)updateSessionUserState:(AIDSessionUserState *)userState;

@property (nonatomic, weak) id <AIDSessionControllerDelegate> delegate;
@end

NS_ASSUME_NONNULL_END
