//
//  AIDUserInputController.h
//
//  Created by Appier on 2020/2/6.
//  Copyright © 2020 Appier. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface AIDTouchTrackingController : NSObject
- (NSArray *)getUserInput;
- (void)startTracking;
- (void)stopTrackingAndClean;
- (void)cleanInput;
- (void)registerScrollView:(UIScrollView *)scrollView;
@end

NS_ASSUME_NONNULL_END


