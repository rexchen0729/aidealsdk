//
//  AIDConfigCampaignManager.h
//
//  Created by Appier on 2020/2/4.
//  Copyright © 2020 Appier. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AIDConfigCampaign.h"

NS_ASSUME_NONNULL_BEGIN

@interface AIDConfigCampaignManager : NSObject
+ (instancetype)instance;
- (void)fetchAiDealConfigAndCampaignsWithApiKey:(NSString *)apiKey CompletionHandler:(void (^)(bool success))completionHandler;
- (nullable AIDConfig *)getConfigByElementId:(NSNumber *)elementId;
- (nullable AIDCampaign *)getCampaignByElementId:(NSNumber *)elementId;
- (void)updateConfig:(AIDConfig *)inAppForUpdate;
- (void)resetCountDownOfAllCampaigns;
@end

@interface AIDEvent : NSObject
+ (NSString *)Eventname_SaveCampaign;
+ (NSString *)Eventname_UserInput;
+ (NSString *)Eventname_Conversion;
@end
NS_ASSUME_NONNULL_END
