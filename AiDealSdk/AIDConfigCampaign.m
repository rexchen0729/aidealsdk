//
//  AIDConfig.m
//
//  Created by Appier on 2020/2/4.
//  Copyright © 2020 Appier. All rights reserved.
//

#import "AIDConfigCampaign.h"
#import "AIDUtility.h"
#import "AIDImageManager.h"

#define DEFAULT_QA_MODE_CD 90 //90 secs for QA

@implementation AIDConfig

- (id)initWithData:(NSDictionary *)campaign {
    
    self = [super init];
    if (!self) return nil;
        
    if (![AIDUtility isValidObject:campaign]) return nil;

    NSDictionary *data = campaign;
    
    if ([AIDUtility isValidObject:[data objectForKey:@"campaign_id"]]) {
        self.campaignId = [data objectForKey:@"campaign_id"];
    }
    
    if ([AIDUtility isValidObject:[data objectForKey:@"element_id"]]) {
        self.elementId = [data objectForKey:@"element_id"];
    }
    
    if ([AIDUtility isValidObject:[data objectForKey:@"campaign_control_percent"]]) {
        self.campaignControlPercent = @([[data objectForKey:@"campaign_control_percent"] intValue]);
        self.campaignControlPercent = @(30); //Tmp
    }
    
    if ([AIDUtility isValidObject:[data objectForKey:@"countdown_lang"]]) {
        self.lang = [data objectForKey:@"countdown_lang"];
    }
  
    if ([data objectForKey:@"variables"]) {
        
        NSDictionary *variables = [data objectForKey:@"variables"];
        
        if ([AIDUtility isValidObject:[variables objectForKey:@"badge_icon_url"]]) {
            self.badgeUrl = [variables objectForKey:@"badge_icon_url"];
            [self downloadImageFromImageManager:self.badgeUrl];
        }
            
        if ([AIDUtility isValidObject:[variables objectForKey:@"notice"]]) {
            self.badgeText = [variables objectForKey:@"notice"];
        }

        if ([AIDUtility isValidObject:[variables objectForKey:@"base_color"]]) {
            self.badgeBaseColor = [variables objectForKey:@"base_color"];
        }
        
        if ([AIDUtility isValidObject:[variables objectForKey:@"limit"]]) {
            self.limit = [variables objectForKey:@"limit"];
        }
        
        if ([AIDUtility isValidObject:[variables objectForKey:@"enables_timer"]]) {
            self.enableTimer = [variables objectForKey:@"enables_timer"];
        }
    }
    
    self.countDownStartTimestamp = [NSNumber numberWithLongLong:0];
    
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder {
    
    [aCoder encodeObject:self.campaignId forKey:@"aid_campaignid"];
    [aCoder encodeObject:self.elementId forKey:@"aid_elementid"];
    [aCoder encodeObject:self.badgeUrl forKey:@"aid_badgeurl"];
    [aCoder encodeObject:self.badgeText forKey:@"aid_badgetext"];
    [aCoder encodeObject:self.badgeBaseColor forKey:@"aid_badgebasecolor"];
    [aCoder encodeObject:self.lang forKey:@"aid_badgelang"];
    [aCoder encodeObject:self.countDownStartTimestamp forKey:@"aid_countDownStartTimestamp"];
    [aCoder encodeObject:self.limit forKey:@"aid_limit"];
    [aCoder encodeObject:self.enableTimer forKey:@"aid_enabletimer"];
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    
    if(self = [self init]) {
        _campaignId = [aDecoder decodeObjectForKey:@"aid_campaignid"];
        _elementId = [aDecoder decodeObjectForKey:@"aid_elementid"];
        _badgeUrl = [aDecoder decodeObjectForKey:@"aid_badgeurl"];
        _badgeText = [aDecoder decodeObjectForKey:@"aid_badgetext"];
        _badgeBaseColor = [aDecoder decodeObjectForKey:@"aid_badgebasecolor"];
        _lang = [aDecoder decodeObjectForKey:@"aid_badgelang"];
        _countDownStartTimestamp = [aDecoder decodeObjectForKey:@"aid_countDownStartTimestamp"];
        _limit = [aDecoder decodeObjectForKey:@"aid_limit"];
        _enableTimer = [aDecoder decodeObjectForKey:@"aid_enabletimer"];
    }
    return self;
}

- (BOOL)isControl {
    if ([[AIDUtility sharedManager] getUserDefaultsForKey:@"AID_QA_MODE_GROUP_CONTROL"]) {
        return YES;
    }
    return _isControl;
}

+ (int)totoalCountDownPeriodFromConfig:(AIDConfig *)config {
    if ([[AIDUtility sharedManager] getUserDefaultsForKey:@"AID_QA_MODE"]) {
        return DEFAULT_QA_MODE_CD;
    }
    return [config.limit intValue] * 60;
}

+ (AIDConfigLANG)checkLangType:(NSString *)lang {
    if ([AIDUtility isValidObject:lang] == NO) {
        return AIDConfigLANG_Universal;
    }
    if ([[lang lowercaseString] isEqualToString:@"jp"]) {
        return AIDConfigLANG_JP;
    } else if ([[lang lowercaseString] isEqualToString:@"en"]) {
        return AIDConfigLANG_EN;
    }
    return AIDConfigLANG_Universal;
}

//----if there is any image in the config, simultaneously download and save the image in the file manager----
- (void)downloadImageFromImageManager:(NSString *)urlString {
    
    [[AIDImageManager instance] downloadImageForURL:urlString];
}

- (void)resetTime {
    
    self.countDownStartTimestamp = [NSNumber numberWithLongLong:0];
}
@end

@implementation AIDCampaign

- (id)initWithData:(NSDictionary *)campaign {
    
    self = [super init];
    if (!self) return nil;
        
    if (![AIDUtility isValidObject:campaign]) return nil;

    NSDictionary *data = campaign;
    
    if ([AIDUtility isValidObject:[data objectForKey:@"css"]]) {
        self.css = [data objectForKey:@"css"];
    }
    if ([AIDUtility isValidObject:[data objectForKey:@"views"]]) {
        
        NSDictionary *views = [data objectForKey:@"views"];
        if ([AIDUtility isValidObject:[views objectForKey:@"offer"]]) {
            self.offer = [views objectForKey:@"offer"];
        }
        
        if ([AIDUtility isValidObject:[views objectForKey:@"present"]]) {
            self.present = [views objectForKey:@"present"];
        }
    }

    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder {
    
    [aCoder encodeObject:self.elementId forKey:@"aid_elementid"];
    [aCoder encodeObject:self.css forKey:@"aid_css"];
    [aCoder encodeObject:self.offer forKey:@"aid_offer"];
    [aCoder encodeObject:self.present forKey:@"aid_present"];
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    
    if(self = [self init]) {

        _elementId = [aDecoder decodeObjectForKey:@"aid_elementid"];
        _css = [aDecoder decodeObjectForKey:@"aid_css"];
        _offer = [aDecoder decodeObjectForKey:@"aid_offer"];
        _present = [aDecoder decodeObjectForKey:@"aid_present"];
    }
    return self;
}
@end
