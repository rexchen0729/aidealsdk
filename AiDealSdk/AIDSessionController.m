//
//  AIDSession.m
//
//  Created by Appier on 2020/2/17.
//  Copyright © 2020 Appier. All rights reserved.
//

#import "AIDSessionController.h"
#import "AIDUtility.h"

#define AID_SESSION_INFO @"AID_SEESION_INFO"
#define AID_SESSION_PAGE_INFO @"AID_SESSION_PAGE_INFO"
#define AID_SESSION_INACTIVE_START_TIMESTAMP @"AID_SESSION_INACTIVE_START_TIMESTAMP"
#define AID_SEESION_RECEIVED_CAMPAIGN @"AID_SEESION_RECEIVED_CAMPAIGN"
#define AID_SESSION_USER_STATE @"AID_SESSION_USER_STATE"
#define AID_SESSION_QA_MODE_INACTIVE_TIMEOUT 120 //120 secs for QA
#define AID_SESSION_INACTIVE_TIMEOUT 3600 * 2 //2 hours for Production

// ** Stored Data schema *****************************************************
// AIDSessionInfo
// * AIDPageInfo *pageInfo [ uuid, usid, sid, device ... ]
// * AIDSessionUserState *userState [ showedOfferView, showedPresentView, countDownEnded ... ]
// * NSDictionary *receivedCampaign
// * NSString *sessionInActivityStartTimestamp
// ***************************************************************************

@implementation AIDSessionController
- (instancetype)init {
    
    self = [super init];
    [[NSNotificationCenter defaultCenter] addObserver:self
                       selector:@selector(appWillTerminate:)
                           name:UIApplicationWillTerminateNotification
                         object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                      selector:@selector(appDidEnterBackground:)
                          name:UIApplicationDidEnterBackgroundNotification
                        object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                       selector:@selector(appWillEnterForeground:)
                           name:UIApplicationWillEnterForegroundNotification
                         object:nil];
    return self;
}

- (BOOL)isSessionTimeout {
    
    AIDSessionInfo *sessionInfo = [[AIDUtility sharedManager] getDecodedDataForKey:AID_SESSION_INFO];
    if (sessionInfo) {
        if (sessionInfo.sessionInActivityStartTimestamp && sessionInfo.sessionInActivityStartTimestamp.length) {
            long long start = [sessionInfo.sessionInActivityStartTimestamp longLongValue];
            long long end = (long long)[NSDate new].timeIntervalSince1970;
            long long sessionTimeout = AID_SESSION_INACTIVE_TIMEOUT;
            if ([[AIDUtility sharedManager] getUserDefaultsForKey:@"AID_QA_MODE"]) {
                sessionTimeout = AID_SESSION_QA_MODE_INACTIVE_TIMEOUT;
            }
            if ((end - start) > sessionTimeout) {
                NSLog(@"Session Timeout");
                return YES;
            }
        }
    }
    return NO;
}

- (void)storePageInfo:(NSDictionary *)newPageInfo {
    
    if (newPageInfo) {
        AIDPageInfo *pageInfo = [[AIDPageInfo alloc] initWithDictinary:newPageInfo];
        
        NSLog(@":::: StorePageInfo :::: \n sid:%@\n usid:%@\n uuid:%@", pageInfo.sid, pageInfo.usid, pageInfo.uuid);
        
        AIDSessionInfo *sessionInfo = [[AIDUtility sharedManager] getDecodedDataForKey:AID_SESSION_INFO];
        if (sessionInfo == nil) {
            sessionInfo = [AIDSessionInfo new];
        }
        sessionInfo.pageInfo = pageInfo;
        [[AIDUtility sharedManager] saveEncodedData:sessionInfo forKey:AID_SESSION_INFO];
    }
}

- (NSDictionary *)getPageInfoAsDictionary {
    
    AIDSessionInfo *sessionInfo = [[AIDUtility sharedManager] getDecodedDataForKey:AID_SESSION_INFO];
    if (sessionInfo && sessionInfo.pageInfo) {
        NSMutableDictionary *outPageInfo = [NSMutableDictionary new];
        if (sessionInfo.pageInfo.uuid) {
            [outPageInfo setObject:sessionInfo.pageInfo.uuid forKey:@"uuid"];
        }
        if (sessionInfo.pageInfo.usid) {
            [outPageInfo setObject:sessionInfo.pageInfo.usid forKey:@"usid"];
        }
        if (sessionInfo.pageInfo.sid) {
            [outPageInfo setObject:sessionInfo.pageInfo.sid forKey:@"sid"];
        }
        if (sessionInfo.pageInfo.device) {
            [outPageInfo setObject:sessionInfo.pageInfo.device forKey:@"device"];
        }
        if (sessionInfo.pageInfo.itemPrice) {
            [outPageInfo setObject:sessionInfo.pageInfo.itemPrice forKey:@"item_price"];
        }
        if (sessionInfo.pageInfo.cartPrice) {
            [outPageInfo setObject:sessionInfo.pageInfo.cartPrice forKey:@"cart_price"];
        }
        return outPageInfo;
    }
    return nil;
}

//----clean sid field when we move to other pages---
- (void)cleanPageInfoHistory {
    
    AIDSessionInfo *sessionInfo = [[AIDUtility sharedManager] getDecodedDataForKey:AID_SESSION_INFO];
    if (sessionInfo) {
        sessionInfo.pageInfo.sid = @"";
        [[AIDUtility sharedManager] saveEncodedData:sessionInfo forKey:AID_SESSION_INFO];
    }
}

//----clean necesary field when session timeout---
- (void)cleanSessionInfo {

    AIDSessionInfo *sessionInfo = [[AIDUtility sharedManager] getDecodedDataForKey:AID_SESSION_INFO];
    if (sessionInfo) {
        sessionInfo.pageInfo.usid = @"";
        sessionInfo.pageInfo.sid = @"";
        sessionInfo.sessionInActivityStartTimestamp = @"";
        sessionInfo.receivedCampaign = nil;
        [[AIDUtility sharedManager] saveEncodedData:sessionInfo forKey:AID_SESSION_INFO];
        NSLog(@":::: CleanSessionInfo ::::\n");
    }
}

//----clean all field. Currently it is used by QA---
- (void)cleanSessionInfoStorage {
    
    [[AIDUtility sharedManager] removeUserDefaultsForKey:AID_SESSION_INFO];
}

//---extend session time---
- (void)extendSession {
    
    AIDSessionInfo *sessionInfo = [[AIDUtility sharedManager] getDecodedDataForKey:AID_SESSION_INFO];
    if (sessionInfo) {
        sessionInfo.sessionInActivityStartTimestamp = @"";
        [[AIDUtility sharedManager] saveEncodedData:sessionInfo forKey:AID_SESSION_INFO];
        NSLog(@":::: ExtendSessionInfo ::::\n");
    }
}

- (void)storeReceivedCampaignEvent:(NSDictionary *)campaignEvent {
    
    if (campaignEvent) {
        AIDSessionInfo *sessionInfo = [[AIDUtility sharedManager] getDecodedDataForKey:AID_SESSION_INFO];
        if (sessionInfo == nil) {
            sessionInfo = [AIDSessionInfo new];
        }
        AIDReceiveCampaignEvent *receivedCampaign = [[AIDReceiveCampaignEvent alloc] initWithDictinary:campaignEvent];
        sessionInfo.receivedCampaign = receivedCampaign;
        [[AIDUtility sharedManager] saveEncodedData:sessionInfo forKey:AID_SESSION_INFO];
    }
}

- (nullable AIDReceiveCampaignEvent *)getStoredReceivedCampaginEvent {
    
    AIDSessionInfo *sessionInfo = [[AIDUtility sharedManager] getDecodedDataForKey:AID_SESSION_INFO];
    if (sessionInfo) {
        return sessionInfo.receivedCampaign;
    }
    return nil;
}

- (nullable NSDictionary *)getStoredReceivedCampaginEventForEvent {
    
    AIDReceiveCampaignEvent *receiveCampaignEvt = [self getStoredReceivedCampaginEvent];
    if (receiveCampaignEvt) {
        NSMutableDictionary *params = [NSMutableDictionary new];
        
        //campaign_id <!String>: Pass campaign_id received from ReceiveCampaign event (Required)
        if (receiveCampaignEvt.campaign_id) {
            [params setObject:receiveCampaignEvt.campaign_id forKey:@"campaign_id"];
        }
        
        //element_id <!String>: Pass element_id received from ReceiveCampaign event (Required)
        if (receiveCampaignEvt.element_id) {
            [params setObject:receiveCampaignEvt.element_id forKey:@"element_id"];
        }
        
        //cron_time <Number>: Pass cron_time received from ReceiveCampaign event
        if (receiveCampaignEvt.cron_time) {
            [params setObject:receiveCampaignEvt.cron_time forKey:@"cron_time"];
        }
        
        //policy_id <Number>: Pass policy_id received from ReceiveCampaign event
        if (receiveCampaignEvt.policy_id) {
            [params setObject:receiveCampaignEvt.policy_id forKey:@"policy_id"];
        }
        
        //coupon_code <String>: Pass coupon_code received from ReceiveCampaign event
        if (receiveCampaignEvt.coupon_code) {
            [params setObject:receiveCampaignEvt.coupon_code forKey:@"coupon_code"];
        }
        
        //incentive_id <Number>: Pass incentive_id received from ReceiveCampaign event
        if (receiveCampaignEvt.incentive_id) {
            [params setObject:receiveCampaignEvt.incentive_id forKey:@"incentive_id"];
        }
        
        //incentive_type <!String>: Pass incentive_type received from ReceiveCampaign event
        if (receiveCampaignEvt.incentive_type) {
            [params setObject:receiveCampaignEvt.incentive_type forKey:@"incentive_type"];
        }
        
        //is_information_campaign <!Boolean>: Always pass `false` for now
        [params setObject:[NSNumber numberWithBool:NO] forKey:@"is_information_campaign"];
        
        //triggers <!Array<!String>>: Always pass `[]` for now
        [params setObject:[NSArray new] forKey:@"triggers"];
        
        [params setObject:@(0) forKey:@"is_information_campaign"];
        [params setObject:@[] forKey:@"triggers"];
        return params;
    }
    return nil;
}

- (void)removeStoredReceivedCampaginEvent {
    
    AIDSessionInfo *sessionInfo = [[AIDUtility sharedManager] getDecodedDataForKey:AID_SESSION_INFO];
    if (sessionInfo) {
        sessionInfo.receivedCampaign = [AIDReceiveCampaignEvent new];
        [[AIDUtility sharedManager] saveEncodedData:sessionInfo forKey:AID_SESSION_INFO];
    }
}

- (nullable NSNumber *)getStoredElementId {
    
    AIDReceiveCampaignEvent *campaignEvent = [self getStoredReceivedCampaginEvent];
    if (campaignEvent) {
        id element_id = campaignEvent.element_id;
        if ([AIDUtility isValidObject:element_id]) {
            if ([element_id isKindOfClass:[NSString class]]) {
                return [NSNumber numberWithInteger:[element_id integerValue]];
            } else {
                return element_id;
            }
        }
    }
    return nil;
}

- (nullable AIDSessionUserState *)getStoredSessionUserState {
    
    AIDSessionInfo *sessionInfo = [[AIDUtility sharedManager] getDecodedDataForKey:AID_SESSION_INFO];
    if (sessionInfo) {
        return sessionInfo.userState;
    }
    return nil;
}

- (BOOL)isPageInfoReady {
    
    AIDSessionInfo *sessionInfo = [[AIDUtility sharedManager] getDecodedDataForKey:AID_SESSION_INFO];
    if (sessionInfo) {
        AIDPageInfo *pageInfo = sessionInfo.pageInfo;
        if (pageInfo) {
            NSString *uuid = pageInfo.uuid;
            NSString *usid = pageInfo.usid;
            NSString *sid = pageInfo.sid;
            if (uuid && uuid.length && usid && usid.length && sid && sid.length) {
                return YES;
            }
        }
    }
    return NO;
}

- (BOOL)isUserInputSuppressed {
    
    AIDSessionInfo *sessionInfo = [[AIDUtility sharedManager] getDecodedDataForKey:AID_SESSION_INFO];
    if (sessionInfo) {
        AIDPageInfo *pageInfo = sessionInfo.pageInfo;
        return pageInfo.behaviorSuppressed;
    }
    return NO;
}

- (AIDSessionInfo *)getStoredSessionInfo {
    
    AIDSessionInfo *sessionInfo = [[AIDUtility sharedManager] getDecodedDataForKey:AID_SESSION_INFO];
    return sessionInfo;
}

- (void)updateSessionUserState:(AIDSessionUserState *)userState {
 
    if (userState) {
        AIDSessionInfo *sessionInfo = [[AIDUtility sharedManager] getDecodedDataForKey:AID_SESSION_INFO];
        if (sessionInfo == nil) {
            sessionInfo = [AIDSessionInfo new];
        }
        sessionInfo.userState = userState;
        [[AIDUtility sharedManager] saveEncodedData:sessionInfo forKey:AID_SESSION_INFO];
    }
}

- (void)setInactivityTimestamp {
    
    if ([self isPageInfoReady]) {
        AIDSessionInfo *sessionInfo = [[AIDUtility sharedManager] getDecodedDataForKey:AID_SESSION_INFO];
        if (sessionInfo) {
            NSString *nowTimestamp = [NSString stringWithFormat:@"%lld", (long long)[NSDate new].timeIntervalSince1970];
#if NEW_STAGING
            NSLog(@"set Inactivity Timestamp:%@", nowTimestamp);
#endif
            sessionInfo.sessionInActivityStartTimestamp = nowTimestamp;
            [[AIDUtility sharedManager] saveEncodedData:sessionInfo forKey:AID_SESSION_INFO];
        }
    }
}

- (void)reportWhenSocketDisconnected {

    [self setInactivityTimestamp];
}

#pragma mark - Application Event Observation
- (void)appWillTerminate:(NSNotification *)notification {

    [self setInactivityTimestamp];
}

- (void)appDidEnterBackground:(NSNotification *)notification {

    [self setInactivityTimestamp];
}

- (void)appWillEnterForeground:(NSNotification *)notification {
    
    [self.delegate sessionResumeFromBackground];
}
@end
