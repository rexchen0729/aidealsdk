//
//  AIDPageType.m
//
//  Created by Appier on 2020/3/2.
//  Copyright © 2020 Appier. All rights reserved.
//

#import "AIDPageType.h"

@implementation AIDPageTypeHelper
+ (NSString *)convertPageToString:(AIDPageType)page {
    
    switch(page) {
        case AIDPageType_Undefined:
            return @"undefined";
            break;
        case AIDPageType_Uninterested:
            return @"";
        case AIDPageType_Top:
            return @"top";
        case AIDPageType_Category:
            return @"category";
        case AIDPageType_Search:
            return @"search";
        case AIDPageType_Item:
            return @"item";
        case AIDPageType_Cart:
            return @"cart";
            break;
        case AIDPageType_CartForm:
            return @"cart_form";
            break;
        case AIDPageType_Conversion:
            return @"conversion";
            break;
        case AIDPageType_MyPage:
            return @"mypage";
            break;
        case AIDPageType_Login:
            return @"login";
            break;
        case AIDPageType_RegistrationForm:
            return @"registration_form";
            break;
        case AIDPageType_Registration:
            return @"registration";
            break;
    }
    return @"unknown";
}

+ (NSDictionary *)preparePageWithType:(AIDPageType)page IsLogin:(BOOL)isLogin ItemPrice:(nullable NSNumber *)itemprice
                            CartPrice:(nullable NSNumber *)cartprice Parms:(nullable NSDictionary*)parms {
    
    NSString *pageName = [AIDPageTypeHelper convertPageToString:page];
    NSMutableDictionary *dic = [NSMutableDictionary new];
    if (parms) {
       dic = [NSMutableDictionary dictionaryWithDictionary:parms];
    }
    if (itemprice) {
        [dic setValue:itemprice forKey:@"item_price"];
    }
    if (cartprice) {
        [dic setValue:cartprice forKey:@"cart_price"];
    }
    [dic setValue:[NSNumber numberWithBool:isLogin] forKey:@"is_login"];
    [dic setValue:@[pageName] forKey:@"page_types"];
    return dic;
}
@end
