//
//  AIDRequestManager.h
//  AiDealSdk
//
//  Created by Appier on 2019/7/16.
//  Copyright © 2019 Appier. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface AIDRequestManager : NSObject

//Get
+ (void)requestInAppCampaigns:(void (^)(NSData * _Nullable data, NSURLResponse *response, NSError * _Nullable error))completionHandler;

//Get
+ (void)requestExitPushDataFromServer:(void (^)(NSData * _Nullable data, NSURLResponse *response, NSError * _Nullable error))completionHandler;

//Get
+ (void)requestPersonalizationConfigFromServer:(void (^)(NSData * _Nullable data, NSURLResponse *response, NSError * _Nullable error))completionHandler;

//Get
+ (void)requestShowPushPrompt:(void (^)(NSData * _Nullable, NSURLResponse * _Nonnull, NSError * _Nullable))completionHandler;

//Post
+ (void)uploadCrashLog:(NSData*) postData CompletionHandler:(void (^)(NSURLResponse *response, NSError * _Nullable error))completionHandler;
@end

NS_ASSUME_NONNULL_END
