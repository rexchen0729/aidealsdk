//
//  AIDSessionInfo.m
//
//  Created by Appier on 2020/2/28.
//  Copyright © 2020 Appier. All rights reserved.
//

#import "AIDSessionInfo.h"
@implementation AIDReceiveCampaignEvent
- (instancetype)initWithDictinary:(NSDictionary *)campaignInfo {
    
    self = [super init];
    if (self) {
        if (campaignInfo && [campaignInfo objectForKey:@"campaign_id"]) {
            self.campaign_id = [campaignInfo objectForKey:@"campaign_id"];
        } else {
            self.campaign_id = [NSNumber numberWithLongLong:0];
        }

        if (campaignInfo && [campaignInfo objectForKey:@"cron_time"]) {
            self.cron_time = [campaignInfo objectForKey:@"cron_time"];
        } else {
            self.cron_time = @"";
        }
        
        if (campaignInfo && [campaignInfo objectForKey:@"incentive_id"]) {
            self.incentive_id = [campaignInfo objectForKey:@"incentive_id"];
        } else {
            self.incentive_id = @"";
        }
        
        if (campaignInfo && [campaignInfo objectForKey:@"incentive_type"]) {
            self.incentive_type = [campaignInfo objectForKey:@"incentive_type"];
        } else {
            self.incentive_type = @"";
        }
        
        if (campaignInfo && [campaignInfo objectForKey:@"policy_id"]) {
            self.policy_id = [campaignInfo objectForKey:@"policy_id"];
        } else {
            self.policy_id = @"";
        }
        
        if (campaignInfo && [campaignInfo objectForKey:@"element_id"]) {
            self.element_id = [campaignInfo objectForKey:@"element_id"];
        } else {
            self.element_id = [NSNumber numberWithLongLong:0];
        }
        
        if (campaignInfo && [campaignInfo objectForKey:@"name"]) {
            self.name = [campaignInfo objectForKey:@"name"];
        } else {
            self.name = @"";
        }
        
        if (campaignInfo && [campaignInfo objectForKey:@"coupon_code"]) {
            self.coupon_code = [campaignInfo objectForKey:@"coupon_code"];
        } else {
            self.coupon_code = @"";
        }
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    
    if (self = [self init]) {
        _campaign_id = [aDecoder decodeObjectForKey:@"aid_received_campaign_campaign_id"];
        _cron_time = [aDecoder decodeObjectForKey:@"aid_cron_time"];
        _incentive_id = [aDecoder decodeObjectForKey:@"aid_incentive_id"];
        _incentive_type = [aDecoder decodeObjectForKey:@"aid_incentive_type"];
        _policy_id = [aDecoder decodeObjectForKey:@"aid_policy_id"];
        _element_id = [aDecoder decodeObjectForKey:@"aid_received_campaign_element_id"];
        _name = [aDecoder decodeObjectForKey:@"aid_received_campaign_name"];
        _coupon_code = [aDecoder decodeObjectForKey:@"aid_received_campaign_coupon_code"];
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder {
    
    [aCoder encodeObject:self.campaign_id forKey:@"aid_received_campaign_campaign_id"];
    [aCoder encodeObject:self.cron_time forKey:@"aid_cron_time"];
    [aCoder encodeObject:self.incentive_id forKey:@"aid_incentive_id"];
    [aCoder encodeObject:self.incentive_type forKey:@"aid_incentive_type"];
    [aCoder encodeObject:self.policy_id forKey:@"aid_policy_id"];
    [aCoder encodeObject:self.element_id forKey:@"aid_received_campaign_element_id"];
    [aCoder encodeObject:self.name forKey:@"aid_received_campaign_name"];
    [aCoder encodeObject:self.coupon_code forKey:@"aid_received_campaign_coupon_code"];
}
@end

@implementation AIDPageInfo
- (instancetype)initWithDictinary:(NSDictionary *)pageInfo {
    
    self = [super init];
    if (self) {
        if (pageInfo && [pageInfo objectForKey:@"uuid"]) {
            self.uuid = [pageInfo objectForKey:@"uuid"];
        }
        if (pageInfo && [pageInfo objectForKey:@"usid"]) {
            self.usid = [pageInfo objectForKey:@"usid"];
        }
        if (pageInfo && [pageInfo objectForKey:@"sid"]) {
            self.sid = [pageInfo objectForKey:@"sid"];
        }
        if (pageInfo && [pageInfo objectForKey:@"device"]) {
            self.device = [pageInfo objectForKey:@"device"];
        } else {
            self.device = @"app";
        }
        if (pageInfo && [pageInfo objectForKey:@"behavior_suppression"]) {
            self.behaviorSuppressed = [[pageInfo objectForKey:@"behavior_suppression"] boolValue];
        }
        if (pageInfo && [pageInfo objectForKey:@"itemPrice"]) {
            self.itemPrice = [NSNumber numberWithInteger:[[pageInfo objectForKey:@"itemPrice"] integerValue]];
        } else {
            self.itemPrice = [NSNumber numberWithInt:0];
        }
        if (pageInfo && [pageInfo objectForKey:@"cartPrice"]) {
            self.cartPrice = [NSNumber numberWithInteger:[[pageInfo objectForKey:@"cartPrice"] integerValue]];
        } else {
            self.cartPrice = [NSNumber numberWithInt:0];
        }
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    
    if (self = [self init]) {
        _uuid = [aDecoder decodeObjectForKey:@"aid_session_uuid"];
        _usid = [aDecoder decodeObjectForKey:@"aid_session_usid"];
        _sid = [aDecoder decodeObjectForKey:@"aid_session_sid"];
        _device = [aDecoder decodeObjectForKey:@"aid_session_device"];
        _itemPrice = [aDecoder decodeObjectForKey:@"aid_session_itemprice"];
        _cartPrice = [aDecoder decodeObjectForKey:@"aid_session_cartPrice"];
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder {
    
    [aCoder encodeObject:self.uuid forKey:@"aid_session_uuid"];
    [aCoder encodeObject:self.usid forKey:@"aid_session_usid"];
    [aCoder encodeObject:self.sid forKey:@"aid_session_sid"];
    [aCoder encodeObject:self.device forKey:@"aid_session_device"];
    [aCoder encodeObject:self.itemPrice forKey:@"aid_session_itemprice"];
    [aCoder encodeObject:self.cartPrice forKey:@"aid_session_cartPrice"];
}
@end

@implementation AIDSessionUserState
- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    
    if (self = [self init]) {
        _showedOfferView = [aDecoder decodeBoolForKey:@"aid_session_showedofferview"];
        _showedPresentView = [aDecoder decodeBoolForKey:@"aid_session_showedpresentview"];
        _countDownEnded = [aDecoder decodeBoolForKey:@"aid_session_countdownendedbeforecardformpage"];
        _visitedCartPage = [aDecoder decodeBoolForKey:@"aid_session_visitedcartpage"];
        _visitedCartFormPageBeforeEvent = [aDecoder decodeBoolForKey:@"aid_session_visitedcartformpagebeforeevent"];
        _sessionState = [aDecoder decodeIntegerForKey:@"aid_session_state"];
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder {
    
    [aCoder encodeBool:self.showedOfferView forKey:@"aid_session_showedofferview"];
    [aCoder encodeBool:self.showedPresentView forKey:@"aid_session_showedpresentview"];
    [aCoder encodeBool:self.countDownEnded forKey:@"aid_session_countdownendedbeforecardformpage"];
    [aCoder encodeBool:self.visitedCartPage forKey:@"aid_session_visitedcartpage"];
    [aCoder encodeBool:self.visitedCartFormPageBeforeEvent forKey:@"aid_session_visitedcartformpagebeforeevent"];
    [aCoder encodeInteger:self.sessionState forKey:@"aid_session_state"];
}
@end

@implementation AIDSessionInfo
- (instancetype)init {
    
    if (self = [super init]) {
        self.pageInfo = [AIDPageInfo new];
        self.userState = [AIDSessionUserState new];
        self.receivedCampaign = [AIDReceiveCampaignEvent new];
        self.sessionInActivityStartTimestamp = @"";
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    
    if (self = [self init]) {
        _pageInfo = [aDecoder decodeObjectForKey:@"aid_pageinfo"];
        _userState = [aDecoder decodeObjectForKey:@"aid_userstate"];
        _receivedCampaign = [aDecoder decodeObjectForKey:@"aid_received_campaignevent"];
        _sessionInActivityStartTimestamp = [aDecoder decodeObjectForKey:@"aid_inActivitystarttimestamp"];
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder {
    
    [aCoder encodeObject:self.pageInfo forKey:@"aid_pageinfo"];
    [aCoder encodeObject:self.userState forKey:@"aid_userstate"];
    [aCoder encodeObject:self.receivedCampaign forKey:@"aid_received_campaignevent"];
    [aCoder encodeObject:self.sessionInActivityStartTimestamp forKey:@"aid_inActivitystarttimestamp"];
}
@end
