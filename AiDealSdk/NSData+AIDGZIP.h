//
//  NSData+AIDGZIP.h
//  QGSdk
//
//  Created by Appier on 30/08/17.
//  Copyright © 2019 APPIER INC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSData (AIDGZIP)

- (nullable NSData *)gzippedDataWithCompressionLevel:(float)level;
- (nullable NSData *)gzippedData;
- (nullable NSData *)gunzippedData;
- (BOOL)isGzippedData;

@end
