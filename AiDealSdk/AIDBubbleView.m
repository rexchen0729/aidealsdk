//
//  AIDBubbleController.m
//
//  Created by Appier on 2020/2/4.
//  Copyright © 2020 Appier. All rights reserved.
//

#import "AIDBubbleView.h"
#import "AIDConfigCampaignManager.h"
#import "AIDImageManager.h"
#import <UIKit/UIKit.h>

const static int kButtonOffsetX = 30; // from left of screen
const static int kButtonOffsetY = 30; // from bottom of screen
const static int kButtonSize = 80;    // Square size

@interface AIDBubbleView()
@property(nonatomic, strong) UIButton *badgeButton;
@property(nonatomic, strong) UILabel *badgeCountDownLabel;
@property(nonatomic, strong) UILabel *badgeNoticeLabel;
@property(nonatomic, strong) UIImageView *badgeImageView;
@property (nonatomic, assign) CGSize screenSize;
@property (nonatomic, assign) CGFloat actualScreenWidth;
@property(nonatomic, strong) NSTimer *countDownTimer;
@property(nonatomic, strong) NSNumber *currentElementId;
@property(nonatomic, copy) NSNumber *currentCountDownStartTimestamp;
@property (nonatomic, assign) BOOL isTapped;
@property (nonatomic, assign) BOOL isDraging;
@property (nonatomic, assign) BOOL bShowCountDownLabel;
@end

@implementation AIDBubbleView
@synthesize badgeButton;
@synthesize badgeCountDownLabel;
@synthesize badgeNoticeLabel;
@synthesize badgeImageView;
@synthesize isTapped;
@synthesize isDraging;
@synthesize currentElementId;
@synthesize currentCountDownStartTimestamp;
@synthesize screenSize;
@synthesize actualScreenWidth;
@synthesize bShowCountDownLabel;

- (instancetype)init {
    
    self = [super init];
    screenSize = [UIScreen mainScreen].bounds.size;
    actualScreenWidth = screenSize.width < screenSize.height ? screenSize.width : screenSize.height;
    //---notifier when screen rotates---
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didRotate:) name:UIApplicationDidChangeStatusBarOrientationNotification object:nil];
    return self;
}

- (void)showBubbleByState:(AIDSessionState)state ElementId:(NSNumber *)elementId {
 
    if (state == AIDSessionState_Undefined || state == AIDSessionState_Disable) {
        [self removeBubble];
        return;
    }
    
    bShowCountDownLabel = (state == AIDSessionState_ShowPresent) ? NO:YES;
    
    if (badgeButton == nil) {
        [self createBadgeBubbleByElementId:elementId];
    }
    
    if (state == AIDSessionState_ShowPresent) {
        [self stopCountDownTimer];
    }
    
    // Could be hidden when a modeless view presents
    badgeButton.hidden = NO;
    
    // Show / hide the countdown label
    badgeCountDownLabel.hidden = !bShowCountDownLabel;
    
    // Show / hide the notice label
    badgeNoticeLabel.hidden = bShowCountDownLabel;
}

- (void)createBadgeBubbleByElementId:(NSNumber *)elementId {
    
    AIDConfig *config = [[AIDConfigCampaignManager instance] getConfigByElementId:elementId];
    if (config) {
        [self removeBubble];
        [self createBubble:config];
        currentElementId = elementId;
    }
}

- (void)createBubble:(AIDConfig *)config {
    
    if (config) {

        // Screen size
        CGRect screenSize = UIScreen.mainScreen.bounds;
        
        // The button
        CGRect rect = CGRectMake(kButtonOffsetX, screenSize.size.height - kButtonSize - kButtonOffsetY, kButtonSize, kButtonSize);
        badgeButton = [UIButton buttonWithType:UIButtonTypeCustom];
        badgeButton.frame = rect;
        badgeButton.layer.masksToBounds = YES;
        badgeButton.layer.cornerRadius = kButtonSize / 5;
        badgeButton.layer.borderColor = [UIColor lightGrayColor].CGColor;
        
        // Control listener
        [badgeButton addTarget:self action:@selector(wasDragged:withEvent:)
                    forControlEvents:UIControlEventTouchDragInside | UIControlEventTouchDragOutside];
        
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(wasTapped:)];
        tap.numberOfTapsRequired = 1;
        [badgeButton addGestureRecognizer:tap];
        [badgeButton addTarget:self action:@selector(dragBegan:withEvent:) forControlEvents: UIControlEventTouchDown];
        [badgeButton addTarget:self action:@selector(dragMoving:withEvent:) forControlEvents: UIControlEventTouchDragInside];
        [badgeButton addTarget:self action:@selector(dragEnded:withEvent:) forControlEvents: UIControlEventTouchUpInside | UIControlEventTouchUpOutside | UIControlEventTouchCancel];
        badgeButton.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleBottomMargin;
             
        [[[UIApplication sharedApplication] keyWindow] addSubview:badgeButton];
        
        // Badge BackgroundColor config.badgeBaseColor
        if (config.badgeBaseColor) {
            badgeButton.backgroundColor = [self getUIColorObjectFromHexString:config.badgeBaseColor alpha:1.0f];
        } else {
            badgeButton.backgroundColor = [UIColor clearColor];
        }
        
        // Badge Image 75%
        int inset = 4;
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, inset, kButtonSize, kButtonSize * 0.75 - inset * 2)];
        imageView.clipsToBounds = true;
        imageView.image = [[AIDImageManager instance] getImageForURL:config.badgeUrl];
        imageView.contentMode = UIViewContentModeScaleAspectFit;
        badgeImageView = imageView;
        [badgeButton addSubview:imageView];
        
        int yOffset = -2;
        // CountDown Text 25% 1/4
        UILabel *cdLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, kButtonSize * 0.75 + yOffset, kButtonSize, kButtonSize * 0.25)];
        badgeCountDownLabel = cdLabel;
        badgeCountDownLabel.textColor = [UIColor whiteColor];
        badgeCountDownLabel.font = [UIFont boldSystemFontOfSize:UIFont.systemFontSize + 1];
        badgeCountDownLabel.textAlignment = NSTextAlignmentCenter;
        [badgeButton addSubview:cdLabel];
        
        // Notice Text 25% 1/4
        UILabel *noticeLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, kButtonSize * 0.75 + yOffset, kButtonSize, kButtonSize * 0.25)];
        badgeNoticeLabel = noticeLabel;
        badgeNoticeLabel.textColor = [UIColor whiteColor];
        badgeNoticeLabel.font = [UIFont boldSystemFontOfSize:UIFont.systemFontSize + 1];
        badgeNoticeLabel.textAlignment = NSTextAlignmentCenter;
        badgeNoticeLabel.text = config.badgeText;
        [badgeButton addSubview:noticeLabel];
    }
}

- (UIColor *)getUIColorObjectFromHexString:(NSString *)hexStr alpha:(CGFloat)alpha
{
    // Convert hex string to an integer
    unsigned int hexint = [self intFromHexString:hexStr];

    // Create a color object, specifying alpha as well
    UIColor *color =
    [UIColor colorWithRed:((CGFloat) ((hexint & 0xFF0000) >> 16))/255
                    green:((CGFloat) ((hexint & 0xFF00) >> 8))/255
                     blue:((CGFloat) (hexint & 0xFF))/255
                    alpha:alpha];

  return color;
}

- (unsigned int)intFromHexString:(NSString *)hexStr {
    unsigned int hexInt = 0;

    // Create scanner
    NSScanner *scanner = [NSScanner scannerWithString:hexStr];

    // Tell scanner to skip the # character
    [scanner setCharactersToBeSkipped:[NSCharacterSet characterSetWithCharactersInString:@"#"]];

    // Scan hex value
    [scanner scanHexInt:&hexInt];

    return hexInt;
}

- (void)showExistingBubble {
    
    if (badgeButton) {
        badgeButton.hidden = NO;
    }
}

- (void)hideExistingBubble {
    
    if (badgeButton) {
         badgeButton.hidden = YES;
    }
}

- (void)removeBubble {

    NSLog(@"Then remove bubble %s", __func__);
    [self stopCountDownTimer];
    if (badgeButton) {
        [badgeButton removeFromSuperview];
        badgeButton = nil;
    }
    if (badgeCountDownLabel) {
        [badgeCountDownLabel removeFromSuperview];
        badgeCountDownLabel = nil;
    }
    if (badgeNoticeLabel) {
        [badgeNoticeLabel removeFromSuperview];
        badgeNoticeLabel = nil;
    }
    self.currentElementId = nil;
}

- (void)startCountDownTimer {
    
    [self stopCountDownTimer];
    AIDConfig *config = [[AIDConfigCampaignManager instance] getConfigByElementId:currentElementId];
    if (config) {
        
        //EnableTimer is false, we don't go forward. And only switch UI to Notice
        if (config.enableTimer.boolValue == NO) {
            badgeCountDownLabel.hidden = YES;
            badgeNoticeLabel.hidden = NO;
            return;
        }
        
        if ([config.countDownStartTimestamp longLongValue] == 0) {
            //config.countDownStartTimestamp = [NSString stringWithFormat:@"%lld", (long long)[NSDate new].timeIntervalSince1970];
            config.countDownStartTimestamp = [NSNumber numberWithLongLong:(long long)[NSDate new].timeIntervalSince1970];
            
        }
        currentCountDownStartTimestamp = config.countDownStartTimestamp;
        [[AIDConfigCampaignManager instance] updateConfig:config];
        
        self.countDownTimer = [NSTimer scheduledTimerWithTimeInterval:0 repeats:YES block:^(NSTimer * _Nonnull timer) {
            long long now = (long long)[[NSDate new] timeIntervalSince1970];
            long long start = [currentCountDownStartTimestamp longLongValue];
            long long remain = [AIDConfig totoalCountDownPeriodFromConfig:config] - (now - start);
            int seconds = remain % 60;
            int minutes = (remain / 60) % 60;
            int hours = ((remain / 60 ) / 60) % 60;
            AIDConfigLANG langType = [AIDConfig checkLangType:config.lang];
            if (langType == AIDConfigLANG_Universal) {
                //AIDConfigLANG_UNIVERSAL
                if (hours > 0) {
                    badgeCountDownLabel.text = [NSString stringWithFormat:@"%0.2d:%0.2d:%.02d", hours, minutes, seconds];
                } else {
                    badgeCountDownLabel.text = [NSString stringWithFormat:@"%0.2d:%.02d", minutes, seconds];
                }
            } else if (langType == AIDConfigLANG_EN) {
                //AIDConfigLANG_EN
                 if (minutes > 0) {
                    badgeCountDownLabel.text = [NSString stringWithFormat:@"%.02dmin", minutes + hours * 60];
                } else {
                    badgeCountDownLabel.text = [NSString stringWithFormat:@"%.02dsec", seconds];
                }
            } else {
                //AIDConfigLANG_JP
                if (minutes > 0) {
                    badgeCountDownLabel.text = [NSString stringWithFormat:@"殘%.02d分", minutes + hours * 60];
                } else {
                    badgeCountDownLabel.text = [NSString stringWithFormat:@"殘%.02d秒", seconds];
                }
            }
            if (remain < 0) {
                [self removeBubble];
                [self.delegate badgeBubbleCountDownEnd];
            }
        }];
    }
}

- (void)stopCountDownTimer {
    
    if (self.countDownTimer) {
        [self.countDownTimer invalidate];
    }
}

///----------------------------------
#pragma mark - Bubble Button Methods
///----------------------------------
- (CGPoint)getButtonCenterWithEvent:(UIEvent *)event {
    
    CGSize screenSize = [UIScreen mainScreen].bounds.size;
    CGPoint center = [[[event allTouches] anyObject] locationInView:[[UIApplication sharedApplication] keyWindow]];
    
    //left
    if (center.x < kButtonSize / 2) {
        center.x = kButtonSize / 2;
    }
    
    //right
    if (center.x > (screenSize.width - kButtonSize / 2)) {
        center.x = screenSize.width - kButtonSize / 2;
    }
    
    //bottom
    if (center.y > (screenSize.height - kButtonSize / 2)) {
        center.y = screenSize.height - kButtonSize / 2;
    }
    
    //top
    if (center.y < kButtonSize / 2) {
        center.y = kButtonSize / 2;
    }
    return center;
}

///----------------------------------
#pragma mark - Screen Rotation
///----------------------------------
- (void)didRotate:(NSNotification *)notification {
    
    CGSize screenSize = UIScreen.mainScreen.bounds.size;
    badgeButton.frame = CGRectMake(kButtonOffsetX, screenSize.height - kButtonSize - kButtonOffsetY, kButtonSize, kButtonSize);
}

- (void)dragBegan:(UIControl *)control withEvent:(UIEvent *)event {
    
    isDraging = NO;
}

- (void)dragMoving:(UIControl *)control withEvent:(UIEvent *)event {
    
    isDraging = YES;
}

- (void)dragEnded:(UIControl *)control withEvent:(UIEvent *)event {
    
    isDraging = NO;
}

//----show and hide message view for tap on bubble----
- (void)wasTapped:(UIButton *)button {
    
    [self.delegate badgeBubbleTapped];
}

- (void)wasDragged:(UIButton *)button withEvent:(UIEvent *)event {
    
    button.center = [self getButtonCenterWithEvent:event];
}
@end

