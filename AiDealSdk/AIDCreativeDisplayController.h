//
//  AIDCreativeDisplayController.h
//
//  Created by Appier on 2020/2/4.
//  Copyright © 2020 Appier. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AIDSessionState.h"

@protocol AIDCreativeDisplayControllerDelegate <NSObject>
- (void)creativeHasDismissed;
- (void)creativeHasPresented;
- (void)creativeHasClickedCouponCode;
@end

NS_ASSUME_NONNULL_BEGIN

@interface AIDCreativeDisplayController : NSObject
- (void)showCreativeBySessionState:(AIDSessionState)state ElementId:(NSNumber *)elementId
                        CouponCode:(NSString *)couponCode CompletionHandler:(void (^)(BOOL success))completionHandler;
@property(nonatomic, weak) id<AIDCreativeDisplayControllerDelegate> delegate;
@end

NS_ASSUME_NONNULL_END
