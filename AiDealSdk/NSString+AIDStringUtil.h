//
//  NSString+AIDStringUtil.h
//  AiDealSdk
//
//  Created by Appier on 01/06/16.
//  Copyright © 2019 APPIER INC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (AIDStringUtil)

- (BOOL)isValidStr;
- (NSURL *)encodedURL;

@end
