//
//  AIDSocketManager.h
//
//  Created by Appier on 2020/2/7.
//  Copyright © 2020 Appier. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol AIDSocketControllerDelegate
- (void)onAIDSocketConnected;
- (void)onAIDSocketDisconnected;
- (void)onAIDSocketReconnected;
- (void)onAIDSocketError:(NSString *)errorMessage;
- (void)onAIDSocketInitPageCompleted:(id)arglist;
- (void)onAIDSocketReceivedCampaign:(id)arglist;
@end

@interface AIDSocketController : NSObject
@property(nonatomic, weak)id<AIDSocketControllerDelegate> delegate;

- (void)connectWithApiKey:(NSString *)apiKey;
- (void)disconnect;
- (BOOL)isConnected;
- (BOOL)initWithPageParams:params;
- (void)sendEventWithName:(NSString *)name Parms:(nullable id)parms;
@end

NS_ASSUME_NONNULL_END
