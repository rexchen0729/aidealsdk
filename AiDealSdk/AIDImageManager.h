//
//  AIDImageManager.h
//  QGSdk
//
//  Created by Appier
//  Copyright (c) 2019 APPIER INC. All rights reserved.
//

#import <Foundation/Foundation.h>

@class UIImage;

@interface AIDImageManager : NSObject

+ (instancetype)instance;
+ (NSURLSession *)session;
- (void)downloadImageForURL:(NSString *)urlString;
- (UIImage *)getImageForURL:(NSString *)urlString;
- (BOOL)isImageAvailableForURL:(NSString *)urlString;
- (void)deleteImageForURL:(NSString *)urlString;
- (void)saveImageToFileManager:(NSData *)data ForURL:(NSString *)url;
@end
