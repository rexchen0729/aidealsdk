//
//  AIDBubbleController.h
//
//  Created by Appier on 2020/6/10.
//  Copyright © 2020 Appier. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AIDSessionState.h"

typedef enum ShowBubbleState : NSUInteger {
    ShowBubbleState_Hide = 0,
    ShowBubbleState_Show
} ShowBubbleState;

@protocol AIDBubbleViewDelegate<NSObject>
- (void)badgeBubbleTapped;
- (void)badgeBubbleCountDownEnd;
@end

NS_ASSUME_NONNULL_BEGIN

@interface AIDBubbleView : NSObject
@property(nonatomic, weak) id<AIDBubbleViewDelegate> delegate;
- (void)showExistingBubble;
- (void)hideExistingBubble;
- (void)removeBubble;
- (void)startCountDownTimer;
- (void)showBubbleByState:(AIDSessionState)state ElementId:(NSNumber *)elementId;
@end

NS_ASSUME_NONNULL_END
