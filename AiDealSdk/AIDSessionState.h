//
//  AIDSessionState.h
//
//  Created by Appier on 2020/3/3.
//  Copyright © 2020 Appier. All rights reserved.
//

#import <Foundation/Foundation.h>
typedef enum AIDSessionState : NSInteger {
    AIDSessionState_Undefined = 0,
    AIDSessionState_ShowOffer,
    AIDSessionState_ShowPresent,
    AIDSessionState_Disable,
} AIDSessionState;

