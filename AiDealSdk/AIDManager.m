//
//  AIDManager.m
//
//  Created by Appier on 2020/2/4.
//  Copyright © 2020 Appier. All rights reserved.
//

#import "AIDManager.h"
#import "AIDConfigCampaignManager.h"
#import "AIDBubbleView.h"
#import "AIDTouchTrackingController.h"
#import "AIDSessionController.h"
#import "AIDSocketController.h"
#import "AIDPageType.h"
#import "AIDSessionState.h"
#import "AIDCreativeDisplayController.h"
#import "AIDUtility.h"
#define DELAY_FOR_PAGE_IDLE 180 // 180 sec / 3 min
#define DELAY_TO_SEND_USERINPUT 3

@interface AIDPageQueueItem : NSObject
@property(nonatomic, assign)AIDPageType type;
@property(nonatomic, strong)UIViewController *viewCtl;
@property(nonatomic, strong)UIScrollView *sclCtl;
@property(nonatomic, copy)NSNumber *itemPrice;
@property(nonatomic, copy)NSNumber *cartPrice;
@property(nonatomic, assign)BOOL isLogin;
@end
@implementation AIDPageQueueItem
@end

@interface AIDManager() <AIDBubbleViewDelegate, AIDCreativeDisplayControllerDelegate, AIDSocketControllerDelegate, AIDSessionControllerDelegate, UINavigationControllerDelegate>
@property(nonatomic, copy) NSString *apiKey;
@property(nonatomic, strong) NSTimer *timer;
@property(nonatomic, weak) UIScrollView *pendingScrollView;
@property(nonatomic, weak) UIScrollView *currentScrollView;
@property(nonatomic, weak) UIViewController *pendingViewCtl;
@property(nonatomic, weak) UIViewController *currentViewCtl;
@property(nonatomic, assign) UIViewController *currnetUnassignedVctl;
@property(nonatomic, copy) NSNumber *currentItemPrice;
@property(nonatomic, copy) NSNumber *currentCartPrice;
@property(nonatomic, copy) NSNumber *pendingItemPrice;
@property(nonatomic, copy) NSNumber *pendingCartPrice;
@property(nonatomic, strong) AIDConversionInfo *conversionInfo;
@property(nonatomic, assign) AIDPageType currentPageType;
@property(nonatomic, strong) AIDBubbleView *bubbleCtl;
@property(nonatomic, strong) AIDTouchTrackingController *userinputCtl;
@property(nonatomic, strong) AIDSocketController *socketCtl;
@property(nonatomic, strong) AIDSessionController *sessionCtl;
@property(nonatomic, strong) AIDCreativeDisplayController *creativeCtl;
@property(nonatomic, assign) AIDPageType pendingPageType;
@property(nonatomic, assign) BOOL pendingIsLogin;
@property(nonatomic, assign) AIDSessionState sessionState;
@property(nonatomic, assign) BOOL bDisconnected;
@property(nonatomic, strong) NSMutableArray *pageInitQueue;
@property(nonatomic, strong) NSMutableArray *pageDeInitQueue;
@property(nonatomic, assign) NSTimeInterval pageStayTimestamp;

//Following state are supposed to be stored in session object
@property(nonatomic, strong) NSNumber *currentElementId;
@property(nonatomic, assign) BOOL bReceivedCampaginEvent;
@property(nonatomic, assign) BOOL bCountDownEnd;
@property(nonatomic, assign) BOOL bShowedOfferView;
@property(nonatomic, assign) BOOL bShowedPresentView;
@property(nonatomic, assign) BOOL bVisitedCartPage;
@property(nonatomic, assign) BOOL bVisitedCartFormPage;
@property(nonatomic, assign) BOOL bVisitedCartFormPageBeforeEvent;
@end

@implementation AIDManager
@synthesize apiKey;
@synthesize timer;
@synthesize pendingScrollView;
@synthesize currentScrollView;
@synthesize pendingViewCtl;
@synthesize currentViewCtl;
@synthesize currnetUnassignedVctl;
@synthesize currentPageType;
@synthesize currentItemPrice;
@synthesize currentCartPrice;
@synthesize pendingItemPrice;
@synthesize pendingCartPrice;
@synthesize pendingIsLogin;
@synthesize conversionInfo;
@synthesize bubbleCtl;
@synthesize userinputCtl;
@synthesize socketCtl;
@synthesize sessionCtl;
@synthesize creativeCtl;
@synthesize pendingPageType;
@synthesize sessionState;
@synthesize bDisconnected;
@synthesize pageInitQueue;
@synthesize pageDeInitQueue;
@synthesize pageStayTimestamp;
@synthesize currentElementId;
@synthesize bReceivedCampaginEvent;
@synthesize bCountDownEnd;
@synthesize bShowedOfferView;
@synthesize bShowedPresentView;
@synthesize bVisitedCartPage;
@synthesize bVisitedCartFormPage;
@synthesize bVisitedCartFormPageBeforeEvent;

+ (instancetype)instance {
    
    static AIDManager *instance_ = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance_ = [[self alloc] init];
    });
    return instance_;
}

#pragma mark - QA
- (void)qaModeEnable {
    [[AIDUtility sharedManager] saveUserDefaultsWithValue:@(1) Key:@"AID_QA_MODE"];
}

- (void)qaModeCleanAllStorage {
    [self qaModeEnable];
    
    //Reset count down
    [[AIDConfigCampaignManager instance] resetCountDownOfAllCampaigns];
    
    //Remove bubble
    [bubbleCtl removeBubble];
    
    //Init / reset default value for all runtime states
    [self resetRuntimeState];
    
    //Clean all storage include pageInfo, userState, and so on
    [sessionCtl cleanSessionInfoStorage];
}

- (void)qaModeCopyAllIDs {
    [self qaModeEnable];
    UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
    NSString *copyBody = @"";
    if (sessionCtl) {
        AIDSessionInfo *sessionInfo = [sessionCtl getStoredSessionInfo];
        if (sessionInfo) {
            copyBody = [copyBody stringByAppendingString:[NSString stringWithFormat:@"uuid:%@ ",  sessionInfo.pageInfo.uuid]];
            copyBody = [copyBody stringByAppendingString:[NSString stringWithFormat:@"usid:%@ ",  sessionInfo.pageInfo.usid]];
            copyBody = [copyBody stringByAppendingString:[NSString stringWithFormat:@"sid:%@ ",  sessionInfo.pageInfo.sid]];
        }
    }
    pasteboard.string = copyBody;
}

- (void)qaModeToggleGroup:(NSNumber *)on {
    [self qaModeEnable];
    [[AIDUtility sharedManager] removeUserDefaultsForKey:@"AID_QA_MODE_GROUP_CONTROL"];
    if (on.boolValue) {
        [[AIDUtility sharedManager] saveUserDefaultsWithValue:@(1) Key:@"AID_QA_MODE_GROUP_CONTROL"];
    }
}

- (BOOL)qaModeIsControlGroup {
    if ([[AIDUtility sharedManager] getUserDefaultsForKey:@"AID_QA_MODE_GROUP_CONTROL"]) {
        return YES;
    }
    return NO;
}

#pragma mark - Init Api / Page / Registration
- (void)initWithApiKey:(NSString *)key {
    
    NSLog(@"Init AID SDK....");
    
    //Init session object
    sessionCtl = [AIDSessionController new];
    sessionCtl.delegate = self;
    
    //Session state is undefined as default
    sessionState = AIDSessionState_Undefined;
    
    //Clean QA Mode first
    [[AIDUtility sharedManager] removeUserDefaultsForKey:@"AID_QA_MODE"];
    
    //ApiKey is used to obstain campaign
    apiKey = key;
    
    //Init / reset default value for all runtime states
    [self resetRuntimeState];
        
    //Present bubble and its behavior
    bubbleCtl = [AIDBubbleView new];
    bubbleCtl.delegate = self;
    
    //Present creative (webview)
    creativeCtl = [AIDCreativeDisplayController new];
    creativeCtl.delegate = self;
    
    //Track user input
    userinputCtl = [AIDTouchTrackingController new];
    
    //Handle socket status, send and receive callback
    socketCtl = [AIDSocketController new];
    socketCtl.delegate = self;
    
    //Page type - uninterested
    currentPageType = AIDPageType_Uninterested;
    
    //Queue
    pageInitQueue = [NSMutableArray new];
    pageDeInitQueue = [NSMutableArray new];
    
    //Fetch config and campaign at init
    [self requestCampaignsFromServer];
    
    //Timer 50ms = 1000ms / 20
    timer = [NSTimer scheduledTimerWithTimeInterval:DELAY_TO_SEND_USERINPUT target:self
                                                         selector:@selector(sendUserInput) userInfo:nil repeats:YES];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appDidBecomeActive:)
                          name:UIApplicationDidBecomeActiveNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appWillTerminate:)
                             name:UIApplicationWillTerminateNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appWillEnterForeground:)
                            name:UIApplicationWillEnterForegroundNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appDidEnterBackground:)
                            name:UIApplicationDidEnterBackgroundNotification object:nil];

}

- (void)initPage:(UIViewController *)vctl PageType:(AIDPageType)pageType Scrollable:(nullable UIScrollView *)scrollable IsLogin:(BOOL)isLogin ItemPrice:(nullable NSNumber *)itemPrice CartPrice:(nullable NSNumber *)cartPrice {
    
    if (pageDeInitQueue.count) {
        
        //Deinit of prev page is not called yet
        AIDPageQueueItem *item = [AIDPageQueueItem new];
        item.type = pageType;
        item.viewCtl = vctl;
        item.sclCtl = scrollable;
        item.itemPrice = itemPrice;
        item.cartPrice = cartPrice;
        item.isLogin = isLogin;
        [pageInitQueue addObject:item];
    } else {
        
        [pageDeInitQueue addObject:[NSNumber numberWithInteger:pageType]];
        [self implTrackPageWithType:pageType ScrollView:scrollable IsLogin:isLogin ViewController:vctl ItemPrice:itemPrice CartPrice:cartPrice];
    }
}

- (void)initializePageDisconnection:(BOOL)disconnect SessionTimeout:(BOOL)timeout {
    
    if (pendingPageType != AIDPageType_Undefined) {
        [self implTrackPageWithType:pendingPageType ScrollView:pendingScrollView IsLogin:pendingIsLogin
                     ViewController:pendingViewCtl ItemPrice:pendingItemPrice CartPrice:pendingCartPrice];
    } else if ((disconnect || timeout) && currentPageType != AIDPageType_Uninterested) {
        if ([socketCtl isConnected]) {
            [socketCtl disconnect];
        }
        [self implTrackPageWithType:currentPageType ScrollView:currentScrollView IsLogin:pendingIsLogin
                     ViewController:currentViewCtl ItemPrice:currentItemPrice CartPrice:currentCartPrice];
    }
}

- (void)implTrackPageWithType:(AIDPageType)pageType ScrollView:(nullable UIScrollView *)scrollView IsLogin:(BOOL)isLogin
               ViewController:(UIViewController *)viewCtl ItemPrice:(NSNumber *)itemPrice CartPrice:(NSNumber *)cartPrice {
        
    if (pageType != AIDPageType_Undefined) {
        if ([socketCtl isConnected]) {
            
            //Clean SID whenever we call init
            [sessionCtl cleanPageInfoHistory];
            
            //If session timeout, change state to AIDState_TrackingNoCheckout first
            [self cleanStateIfSessionTimeout];
            
            //Get campaign id
            if ([sessionCtl getStoredElementId]) {
                currentElementId = [sessionCtl getStoredElementId];
                bReceivedCampaginEvent = YES;
            }
            
            //Get stored session user state back if possible
            AIDSessionUserState *storedUserState = [sessionCtl getStoredSessionUserState];
            if (storedUserState) {
                bCountDownEnd = storedUserState.countDownEnded;
                bVisitedCartPage = storedUserState.visitedCartPage;
                bVisitedCartFormPageBeforeEvent = storedUserState.visitedCartFormPageBeforeEvent;
                bShowedOfferView = storedUserState.showedOfferView;
                bShowedPresentView = storedUserState.showedPresentView;
                sessionState = storedUserState.sessionState;
            }
            
            //Checkout page
            if (pageType == AIDPageType_Cart) {
                bVisitedCartPage = YES;
            }
            
            //Cardform page
            if (pageType == AIDPageType_CartForm) {
                bVisitedCartFormPage = YES;
                if (currentElementId == nil) {
                    bVisitedCartFormPageBeforeEvent = YES;
                }
            }
            
            //Call 'init' socket
            NSLog(@"::: Start tracking with pagetype:%@ :::", [AIDPageTypeHelper convertPageToString:pageType]);
            [socketCtl initWithPageParams:[AIDPageTypeHelper preparePageWithType:pageType IsLogin:isLogin ItemPrice:itemPrice CartPrice:cartPrice Parms:[sessionCtl getPageInfoAsDictionary]]];
        
            //Assign view to the tracking controller <-- tracking userinput
            [userinputCtl registerScrollView:scrollView];
            [userinputCtl stopTrackingAndClean];
            
            //Current pagetype and scrollview
            currentPageType = pageType;
            currentScrollView = scrollView;
            currentViewCtl = viewCtl;
            currentItemPrice = itemPrice;
            currentCartPrice = cartPrice;
            
            //Clean pending state
            pendingPageType = AIDPageType_Undefined;
            pendingScrollView = nil;
            pendingViewCtl = nil;
            pendingItemPrice = nil;
            pendingCartPrice = nil;
            pendingIsLogin = NO;
            
            //Resolve session state - Undefined, ShowOffer, ShowPresent, Disable
            sessionState = [self resolveSessionState:[self getSessionInfo] PageType:currentPageType NowState:sessionState];
            
#if NEW_STAGING
            if (sessionState == AIDSessionState_Undefined) {
                NSLog(@"Current state: Undefined");
            } else if (sessionState == AIDSessionState_ShowOffer) {
                NSLog(@"Current state: ShowOffer");
            } else if (sessionState == AIDSessionState_ShowPresent) {
                NSLog(@"Current state: ShowPresent");
            } else if (sessionState == AIDSessionState_Disable) {
                NSLog(@"Current state: Disable");
            }
#endif
            //Show a badge and a creative based on state
            [self showBubbleAndCreativeByState:sessionState ElementId:[sessionCtl getStoredElementId]
                         ReceivedCampaignEvent:[sessionCtl getStoredReceivedCampaginEvent] Auto:YES];
            
            //Session - save latest state back to storage
            [self saveSessionState];
            
            //Start checking page idle
            [self startPageIdleChecking];
        } else {
            
            //Keep pagetype and view
            pendingPageType = pageType;
            pendingScrollView = scrollView;
            pendingViewCtl = viewCtl;
            pendingItemPrice = itemPrice;
            pendingCartPrice = cartPrice;
            pendingIsLogin = isLogin;
            
            //Connect to server
            [socketCtl connectWithApiKey:apiKey];
        }
    }
}

#pragma mark - Deinit
- (void)deinitPage {

    //Disconnect
    [socketCtl disconnect];
    
    if (pageDeInitQueue.count) {
        NSNumber *typeNum = [pageDeInitQueue firstObject];
        if ((AIDPageType)typeNum.integerValue != currentPageType) {
            NSLog(@"Warning... Init and deinit are not called correspondingly");
        }
        [pageDeInitQueue removeObjectAtIndex:0];
        pendingScrollView = nil;
        pendingViewCtl = nil;
        currentPageType = AIDPageType_Undefined;
        currentScrollView = nil;
        currentViewCtl = nil;
        currentItemPrice = nil;
        currentCartPrice = nil;
        
        if (pageInitQueue.count) {
            AIDPageQueueItem *item = [pageInitQueue firstObject];
            [pageInitQueue removeObjectAtIndex:0];
            [self initPage:item.viewCtl PageType:item.type Scrollable:item.sclCtl IsLogin:item.isLogin
                 ItemPrice:item.itemPrice CartPrice:item.cartPrice];
        }
    }
}

#pragma mark - Init / Deinit UninterestedPage
- (void)initUninterestedPageIfNecessary:(UIViewController *)viewCtl {
    
    //Help init or not
    if (viewCtl && [self isValidUninterestedViewControllForTrack:viewCtl]) {
 
        //Init
        if (viewCtl != pendingViewCtl && viewCtl != currentViewCtl) {
            currnetUnassignedVctl = viewCtl;
            [self initPage:viewCtl PageType:AIDPageType_Uninterested Scrollable:nil IsLogin:NO ItemPrice:nil CartPrice:nil];
        }
    }
}

- (void)deInitUninterestedPageIfNecessary:(UIViewController *)viewCtl {
    
    if (currnetUnassignedVctl && currnetUnassignedVctl != viewCtl && [self isValidUninterestedViewControllForTrack:viewCtl]) {
        [self deinitPage];
        currnetUnassignedVctl = nil;
    }
}

- (BOOL)isValidUninterestedViewControllForTrack:(UIViewController *)viewCtl {
    
    if (viewCtl == nil || [viewCtl isKindOfClass:[UIViewController class]] == NO) {
        return NO;
    }
    
    /*
      We don't care a viewcontroller that is a pageviewcontroller or
      contains a pageviewcontroller. We care each viewcontroller managed by the pageviewcontroller
      PageViewController - (Not care)
         ViewController index:0 - (care if user assign type)
         ViewController index:1 - (care if user assign type)
    */
    if ([viewCtl isKindOfClass:[UIPageViewController class]]) {
        return NO;
    }
    for (int i = 0; i < viewCtl.childViewControllers.count; i++) {
        id childvctl = [viewCtl.childViewControllers objectAtIndex:i];
        if ([childvctl isKindOfClass:[UIPageViewController class]]) {
            return NO;
        }
    }
    return YES;
}

#pragma mark - ConfigCampaign Manager
- (void)requestCampaignsFromServer {
    
    //Fetch config / campaing when we success initialization
    [[AIDConfigCampaignManager instance] fetchAiDealConfigAndCampaignsWithApiKey:apiKey CompletionHandler:^(bool success) {
    }];
}

#pragma mark - Page Idel Checker
- (void)startPageIdleChecking {
    if (sessionState != AIDSessionState_Disable) {
        pageStayTimestamp = [NSDate date].timeIntervalSince1970;
    } else {
        NSLog(@"Already DISABLE ..");
    }
}

- (BOOL)pageIsIdle {
    //If stay idel in the page over idle time, we forcibly change state to DISABLE
    NSTimeInterval now = [NSDate new].timeIntervalSince1970;
    if ((now - pageStayTimestamp) >= DELAY_FOR_PAGE_IDLE) {
        NSLog(@"Page Idel OverTime !! -- > %f", now - pageStayTimestamp);
        return YES;
    }
    return NO;
}

#pragma mark - Badge & Bubble
- (void)showBubbleByState:(AIDSessionState)state ShowedOfferView:(BOOL)showedOfferView ElementId:(NSNumber *)elementId {
    
    [bubbleCtl showBubbleByState:state ElementId:elementId];
    if (showedOfferView  && state == AIDSessionState_ShowOffer) {
        [bubbleCtl startCountDownTimer]; //Just start counting down
    }
}

- (void)showBubbleAndCreativeByState:(AIDSessionState)state ElementId:(NSNumber *)elementId ReceivedCampaignEvent:(AIDReceiveCampaignEvent *)event Auto:(BOOL)bAuto {
    
    // Show Badge
    [self showBubbleByState:state ShowedOfferView:bShowedOfferView ElementId:elementId];
        
    // Coupon code from a received campaign event
    NSString *couponCode = @"";
    if ([AIDUtility isValidObject:event] && [AIDUtility isValidObject:event.coupon_code]) {
        couponCode = event.coupon_code;
    }
    
    // Show Creative
    if (bAuto) {
        if (state == AIDSessionState_ShowOffer && bShowedOfferView == NO) {
            [self showCreativeByState:sessionState ElementId:elementId CouponCode:couponCode CompletionHandler:^(BOOL success) {
                if (success) {
                    bShowedOfferView = YES;

                    //Session - save latest state back to storage
                    [self saveSessionState];
                }
            }];
                
        }
        if (state == AIDSessionState_ShowPresent && bShowedPresentView == NO) {
            [self showCreativeByState:sessionState ElementId:elementId CouponCode:couponCode CompletionHandler:^(BOOL success) {
                if (success) {
                    bShowedPresentView = YES;
                        
                    //Session - save latest state back to storage
                    [self saveSessionState];
                }
            }];
        }
    } else {
        
        [self showCreativeByState:state ElementId:elementId CouponCode:couponCode CompletionHandler:nil];
    }
}

#pragma mark - Show WebView: .OfferView /.PresentView
- (void)showCreativeByState:(AIDSessionState)state ElementId:(NSNumber *)elementId CouponCode:(NSString *)couponCode CompletionHandler:(void (^)(BOOL success))completionHandler  {
    
    [creativeCtl showCreativeBySessionState:state ElementId:elementId CouponCode:couponCode CompletionHandler:^(BOOL success) {
        if (completionHandler) {
            completionHandler(success);
        }
    }];
}

#pragma mark - User Input Timer
- (void)sendUserInput {
    
    if ([self isValidToSendBehavior]) {
        NSArray<NSArray*> *userInputList = [userinputCtl getUserInput];
        if (userInputList && userInputList.count) {
            [socketCtl sendEventWithName:[AIDEvent Eventname_UserInput] Parms:userInputList];
        }
    }
    [userinputCtl cleanInput];
}

#pragma mark - RunTime State
- (void)resetRuntimeState {

    bShowedOfferView = NO;
    bShowedPresentView = NO;
    bVisitedCartPage = NO;
    bVisitedCartFormPageBeforeEvent = NO;
    bCountDownEnd = NO;
    sessionState = AIDSessionState_Undefined;
    bReceivedCampaginEvent = NO;
    currentElementId = nil;
    pendingPageType = AIDPageType_Undefined;
    pendingScrollView = nil;
}

#pragma mark - Session State
- (AIDSessionInfo *)getSessionInfo {
    
    AIDSessionInfo *storedSessioInfo = [sessionCtl getStoredSessionInfo];
    if (storedSessioInfo == nil) {
        storedSessioInfo = [AIDSessionInfo new];
    }
    storedSessioInfo.userState.showedOfferView = bShowedOfferView;
    storedSessioInfo.userState.showedPresentView = bShowedPresentView;
    storedSessioInfo.userState.visitedCartPage = bVisitedCartPage;
    storedSessioInfo.userState.visitedCartFormPageBeforeEvent = bVisitedCartFormPageBeforeEvent;
    storedSessioInfo.userState.countDownEnded = bCountDownEnd;
    storedSessioInfo.userState.sessionState = sessionState;
    return storedSessioInfo;
}

- (void)saveSessionState {
    
    AIDSessionUserState *storedUserState = [sessionCtl getStoredSessionUserState];
    if (storedUserState == nil) {
        storedUserState = [AIDSessionUserState new];
    }
    storedUserState.showedOfferView = bShowedOfferView;
    storedUserState.showedPresentView = bShowedPresentView;
    storedUserState.visitedCartPage = bVisitedCartPage;
    storedUserState.visitedCartFormPageBeforeEvent = bVisitedCartFormPageBeforeEvent;
    storedUserState.countDownEnded = bCountDownEnd;
    storedUserState.sessionState = sessionState;
    [sessionCtl updateSessionUserState:storedUserState];
}

- (BOOL)cleanStateIfSessionTimeout {
    
    if ([sessionCtl isSessionTimeout]) {
        
        //Reset count down
        [[AIDConfigCampaignManager instance] resetCountDownOfAllCampaigns];
        
        //Remove bubble
        [bubbleCtl removeBubble];
        
        //Clean page info, ex: USID, SID, InactivityTimestamp ..
        [sessionCtl cleanSessionInfo];
    
        //Default value of Runtime state
        [self resetRuntimeState];
        
        //Session - save latest state back to storage
        [self saveSessionState];
        
        return YES;
    }
    
    //Extend session if session is not timeout
    [sessionCtl extendSession];
    
    return NO;
}

- (AIDSessionState)resolveSessionState:(AIDSessionInfo *)sessionInfo
                               PageType:(AIDPageType)pageType NowState:(AIDSessionState)nowState {

    if (nowState == AIDSessionState_Disable) {
        return AIDSessionState_Disable;
    }
    if (sessionInfo && sessionInfo.userState) {
        
        AIDSessionUserState *userState = sessionInfo.userState;
        
        if (userState.visitedCartFormPageBeforeEvent) {
            return AIDSessionState_Disable;
        }
        
        if (pageType == AIDPageType_Conversion) {
            return AIDSessionState_Disable;
        }
        
        if (pageType == AIDPageType_CartForm && userState.showedOfferView == NO && userState.showedPresentView == NO) {
            return AIDSessionState_Disable;
        }
        
        if (userState.countDownEnded) {
            return AIDSessionState_Disable;
        }
        
        if (sessionInfo.receivedCampaign.element_id == nil || sessionInfo.receivedCampaign.element_id.longLongValue == 0) {
            return AIDSessionState_Undefined;
        }
        
        if (userState.visitedCartPage && userState.showedOfferView == NO) {
            return AIDSessionState_ShowOffer;
        }
        
        if (userState.visitedCartPage && userState.showedOfferView && userState.showedPresentView == NO) {
            if (pageType == AIDPageType_Cart)
                return AIDSessionState_ShowPresent;
            else
                return AIDSessionState_ShowOffer;
        }
        
        if (userState.visitedCartPage && userState.showedOfferView && userState.showedPresentView) {
            return AIDSessionState_ShowPresent;
        }
        
        if (pageType != AIDPageType_Cart && pageType != AIDPageType_CartForm && nowState != AIDSessionState_ShowPresent) {
            return AIDSessionState_ShowOffer;
        }
        
        if (userState.showedOfferView && userState.showedPresentView) {
            return AIDSessionState_ShowPresent;
        }
        
        // After users see an offerview automatically, they directly go to checkout page
        if (pageType == AIDPageType_CartForm && userState.showedOfferView) {
            return AIDSessionState_ShowPresent;
        }
    }
    return AIDSessionState_Undefined;
}

- (BOOL)isValidToSendBehavior {
    return (apiKey.length && currentPageType != AIDPageType_Uninterested && [socketCtl isConnected] &&
            [sessionCtl isPageInfoReady] && [sessionCtl isUserInputSuppressed] == NO);
}

#pragma mark - Send Event
- (void)sendConversionInfo:(AIDConversionInfo *)Info {
    
    conversionInfo = Info;
    if (currentPageType == AIDPageType_Conversion) {
        NSDictionary *conversionDic = [AIDConversionInfo convertInfoToDictionary:conversionInfo];
        if ([sessionCtl isPageInfoReady] && conversionDic) {
            [socketCtl sendEventWithName:[AIDEvent Eventname_Conversion] Parms:conversionDic];
            
            //Clean cinversionInfo if send already
            conversionInfo = nil;
        }
    }
}

- (void)sendSaveCampaignEvent:(NSDictionary *)saveCampaignDic IsControl:(BOOL)control SID:(NSString *)sid {

    NSMutableDictionary *sendDic = [NSMutableDictionary dictionaryWithDictionary:saveCampaignDic];
    [sendDic setValue:@(control) forKey:@"is_control"];
    [sendDic setValue:sid ? sid : @"" forKey:@"received_sid"];
    NSLog(@"%@", sendDic);
    [socketCtl sendEventWithName:[AIDEvent Eventname_SaveCampaign] Parms:sendDic];
}

#pragma mark - [Delegate] SessionControllerDelegateDelegate
- (void)sessionResumeFromBackground {
    
    //If session timeout, clean or remove some states and objects
    [self initializePageDisconnection:bDisconnected SessionTimeout:[sessionCtl isSessionTimeout]];
}

#pragma mark - [Delegate] BubbleControllerDelegate
- (void)badgeBubbleTapped {

    //Show a badge and a creative based on state
    [self showBubbleAndCreativeByState:sessionState ElementId:[sessionCtl getStoredElementId]
                 ReceivedCampaignEvent:[sessionCtl getStoredReceivedCampaginEvent] Auto:NO];
}

- (void)badgeBubbleCountDownEnd {
    
    bCountDownEnd = YES;
    
    //Resolve session state - Undefined, ShowOffer, ShowPresent, Disable
    sessionState = [self resolveSessionState:[self getSessionInfo] PageType:currentPageType NowState:sessionState];
    
    //Session - save latest state back to storage
    [self saveSessionState];
}

#pragma mark - [Delegate] AIDSocketManagerDelegate
- (void)onAIDSocketConnected {
#if NEW_STAGING
    NSLog(@"Current page :%@", (currentPageType == AIDPageType_Top) ? @"Top" : @"Other Page");
    NSLog(@"Pending page: %@", (pendingPageType == AIDPageType_Top) ? @"Top" : @"Other Page");
#endif
    [self initializePageDisconnection:bDisconnected SessionTimeout:[sessionCtl isSessionTimeout]];
    bDisconnected = NO;
}

- (void)onAIDSocketDisconnected {
    bDisconnected = YES;
    
    //Tell sessionCtl that we are disconnected
    [sessionCtl reportWhenSocketDisconnected];
}

- (void)onAIDSocketReconnected {
}

- (void)onAIDSocketInitPageCompleted:(NSDictionary *)arglist {
    
    //Handle session info - read UUID USID SID and etc.
    [sessionCtl storePageInfo:arglist];
        
    //Start tracking when init completed
    if ([sessionCtl isUserInputSuppressed] == NO) {
        [userinputCtl startTracking];
    }
    
    //Emit conversion if necessary
    [self sendConversionInfo:conversionInfo];
}

- (void)onAIDSocketReceivedCampaign:(NSDictionary *)arglist {
    
    if (bReceivedCampaginEvent == NO && [self pageIsIdle] == NO && arglist) {
        
        //Save the new-coming campaign event
        [sessionCtl storeReceivedCampaignEvent:arglist];
        
        NSNumber *element_id = [sessionCtl getStoredElementId];
        
        AIDConfig *config = [[AIDConfigCampaignManager instance] getConfigByElementId:element_id];
        if (config) {
            
            currentElementId = element_id;

            // Mark that we already receive one campaign and create bubble
            bReceivedCampaginEvent = YES;
            
            //Resolve session state - Undefined, ShowOffer, ShowPresent, Disable
            sessionState = [self resolveSessionState:[self getSessionInfo] PageType:currentPageType NowState:sessionState];
    
            //If is_control, we forcibly change state to DISABLE
            sessionState = (config.isControl) ? AIDSessionState_Disable : sessionState;
            
            //Session - save latest state back to storage
            [self saveSessionState];
            
            if (bVisitedCartPage && currentPageType == AIDPageType_Cart) {
                NSLog(@"Before this received event callback, we have visited and stay a cart page");
            } else {
                [self showBubbleAndCreativeByState:sessionState ElementId:element_id ReceivedCampaignEvent:[sessionCtl getStoredReceivedCampaginEvent] Auto:YES];
            }
            
            //Emit save_campaign
            [self sendSaveCampaignEvent:[sessionCtl getStoredReceivedCampaginEventForEvent] IsControl:config.isControl SID:[sessionCtl getStoredSessionInfo].pageInfo.sid];
        } else {
            
            //Remove
            [sessionCtl removeStoredReceivedCampaginEvent];
        }
        
    } else {
        NSLog(@"Has shown campaign before or Page has been idle:%d", [self pageIsIdle]);
    }
}

- (void)onAIDSocketError:(NSString *)errorMessage {

    NSLog(@"%@", errorMessage);
}

#pragma mark - [Delegate] AIDCreativeDisplayController
- (void)creativeHasDismissed {

    [self showBubbleByState:sessionState ShowedOfferView:bShowedOfferView ElementId:currentElementId];
}

- (void)creativeHasPresented {
    
    [bubbleCtl hideExistingBubble];
}

- (void)creativeHasClickedCouponCode {
    AIDReceiveCampaignEvent* receivedEvent = [sessionCtl getStoredReceivedCampaginEvent];
    if (receivedEvent && [AIDUtility isValidObject:receivedEvent.coupon_code] && receivedEvent.coupon_code.length) {
        NSLog(@"Paste-->%@", receivedEvent.coupon_code);
        UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
        pasteboard.string = receivedEvent.coupon_code;
    }
}

#pragma mark - [Delegate] Navigation Controller
- (void)navigationController:(UINavigationController *)navigationController didShowViewController:(UIViewController *)viewController animated:(BOOL)animated {
    
    [self initUninterestedPageIfNecessary:viewController];
}

- (void)navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated {
    
    [self deInitUninterestedPageIfNecessary:viewController];
}

#pragma mark - Application Event Observation for session state storage
- (void)appDidBecomeActive:(NSNotification *)notification {
    if ([[UIApplication sharedApplication] keyWindow]) {
        [self trackTopViewControllerForUninterestedPage:[[UIApplication sharedApplication] keyWindow]];
    }
}

- (void)appWillTerminate:(NSNotification *)notification {
    
    //Session - save latest state back to storage
    [self saveSessionState];
}

- (void)appDidEnterBackground:(NSNotification *)notification {
    
    //Session - save latest state back to storage
    [self saveSessionState];
}

- (void)appWillEnterForeground:(NSNotification *)notification {
    
    //Request campaign / config whenever go to foreground
    [self requestCampaignsFromServer];
}

#pragma mark - Uninterested page monitoring
- (void)trackTopViewControllerForUninterestedPage:(UIWindow *)window {
    //Register as one navigationController delegate,
    //we may detect some pages / viewcontrollers that are not defined by developers
    if (window) {
        if (window.rootViewController) {
            UIViewController* topViewCtl = window.rootViewController;
            if ([window.rootViewController isKindOfClass:[UINavigationController class]]) {
                UINavigationController *naviCtl = (UINavigationController *)window.rootViewController;
                naviCtl.delegate = self;
                topViewCtl = naviCtl.topViewController;
            }
            [self initUninterestedPageIfNecessary:topViewCtl];
        }
    }
}

@end
