//
//  AIDUserInputController.m
//
//  Created by Appier on 2020/2/6.
//  Copyright © 2020 Appier. All rights reserved.
//

#import "AIDTouchTrackingController.h"
#import "AIDUtility.h"
#define AID_CATCH_GES_DELAY 50 / 1000.0f //50ms

@interface AIDTouchTrackingController() <UIGestureRecognizerDelegate>
@property (nonatomic, assign) NSTimeInterval prevCollectTimestamp;
@property (nonatomic, assign) BOOL isStartTracking;
@property (nonatomic, assign) BOOL isFallbackToUseWindowScroll;
@property (nonatomic, assign) float prevOffsetX;
@property (nonatomic, assign) float prevOffsetY;
@property (nonatomic, weak) UIScrollView *currentScrollView;
@property (nonatomic, strong) NSMutableArray<NSString*> *inputList;
@end

@implementation AIDTouchTrackingController
@synthesize prevCollectTimestamp;
@synthesize isStartTracking;
@synthesize isFallbackToUseWindowScroll;
@synthesize currentScrollView;
@synthesize inputList;
@synthesize prevOffsetX;
@synthesize prevOffsetY;

- (instancetype)init {
    
    self = [super init];
    if (self) {
        inputList = [NSMutableArray new];
    }
    return self;
}

- (void)registerScrollView:(UIScrollView *)scrollView {
    
    isFallbackToUseWindowScroll = YES;
    currentScrollView = scrollView;
    if (currentScrollView) {
        [currentScrollView addObserver:self forKeyPath:@"contentOffset" options:NSKeyValueObservingOptionOld | NSKeyValueObservingOptionNew context:nil];
    }
    prevOffsetX = 0;
    prevOffsetY = 0;
}

#pragma mark - KVO ScrollView ContentOffset
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSKeyValueChangeKey,id> *)change context:(void *)context {
    
    if (isStartTracking && [self isTimeToCollect]) {
        if ([keyPath isEqualToString:@"contentOffset"]) {
            NSValue *oldValue = change[NSKeyValueChangeOldKey];
            NSValue *newValue = change[NSKeyValueChangeNewKey];
            CGFloat oldOffsetY = oldValue.UIOffsetValue.vertical;
            CGFloat newOffsetY = newValue.UIOffsetValue.vertical;
            CGFloat newOffsetX = oldValue.UIOffsetValue.horizontal;
            
            if (newOffsetY > oldOffsetY) {
               // NSLog(@"向下滑动");
            }
            else if(newOffsetY < oldOffsetY) {
               // NSLog(@"向上滑动");
            }
            [self addUserInputWithGesName:@"scroll" ContentW:currentScrollView.contentSize.width ContentH:currentScrollView.contentSize.height
                                     SclX:newOffsetX SclY:newOffsetY DeltaSclX:(newOffsetX - prevOffsetX) DeltaSclY:(newOffsetY - prevOffsetY)];
            
            prevOffsetX = newOffsetX;
            prevOffsetY = newOffsetY;
        } else {
            [super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
        }
    }
}

#pragma mark - UserInput Collection
- (BOOL)isTimeToCollect {

    NSTimeInterval now = AIDCurrentTime().timeIntervalSince1970;
    if ((now - prevCollectTimestamp) > AID_CATCH_GES_DELAY) {
        prevCollectTimestamp = now;
        return YES;
    }
    return NO;
}

- (void)addUserInputWithGesName:(NSString *)gestureName ContentW:(float)contentW
                    ContentH:(float)contentH SclX:(float)SclX SclY:(float)SclY DeltaSclX:(float)delSclX DeltaSclY:(float)delSclY {
    long long now = (long long)([NSDate new].timeIntervalSince1970 * 1000); //millsecond
    NSMutableArray *oneUserInput = [NSMutableArray new];
    [oneUserInput addObject:[NSNumber numberWithLongLong:now]];
    [oneUserInput addObject:@{gestureName:[NSNumber numberWithBool:YES]}];
    [oneUserInput addObject:[NSNumber numberWithFloat:contentH]];
    [oneUserInput addObject:[NSNumber numberWithFloat:contentW]];
    [oneUserInput addObject:[NSNumber numberWithFloat:SclX]];
    [oneUserInput addObject:[NSNumber numberWithFloat:SclY]];
    [oneUserInput addObject:[NSNumber numberWithFloat:delSclX]];
    [oneUserInput addObject:[NSNumber numberWithFloat:delSclY]];
    
    @try {
        NSError *error;
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:oneUserInput options:NSJSONWritingPrettyPrinted error:&error];
        NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        jsonString = [jsonString stringByReplacingOccurrencesOfString:@"\n" withString:@""];
        jsonString = [jsonString stringByReplacingOccurrencesOfString:@" " withString:@""];
        [inputList addObject:jsonString];
#if NEW_STAGING
        NSLog(@"User input JsonString: %@", jsonString);
#endif
    } @catch (NSException *exception) {
        NSLog(@"%@", exception.description);
    }
}

#pragma mark - Gesture Delegate
//*Note: returning YES is guaranteed to allow simultaneous recognition.
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    
    if ([otherGestureRecognizer isKindOfClass:[UITapGestureRecognizer class]]) {
        return NO;
    }
    return YES;
}

#pragma mark - Get and Clean
- (NSArray *)getUserInput {
    
    return inputList;
}

- (void)startTracking {
    
    isStartTracking = YES;
}

- (void)stopTrackingAndClean {

    [self cleanInput];
    isStartTracking = NO;
}

- (void)cleanInput {
    
    if (inputList) {
        [inputList removeAllObjects];
    }
}
@end
