//
//  AIDCreativeDisplayController.m
//
//  Created by Appier on 2020/2/4.
//  Copyright © 2020 Appier. All rights reserved.
//

#import "AIDCreativeDisplayController.h"
#import "AIDWebViewController.h"
#import "AIDConfigCampaignManager.h"
#import <UIKit/UIKit.h>

@interface AIDCreativeDisplayController() <AIDWebViewControllerDelegate>
@property(nonatomic, strong) UIViewController *topInAppWebViewPresenter;
@end

@implementation AIDCreativeDisplayController
@synthesize topInAppWebViewPresenter;

- (void)dismissCreative:(void (^)(void))completeionHandler {
    
    if (topInAppWebViewPresenter) {
         [topInAppWebViewPresenter dismissViewControllerAnimated:NO completion:^{
             [topInAppWebViewPresenter.view removeFromSuperview];
             topInAppWebViewPresenter = nil;
             
             [self.delegate creativeHasDismissed];
             
             if (completeionHandler) {
                 completeionHandler();
             }
         }];
     } else if (completeionHandler) {
         completeionHandler();
     }
}

- (NSString *)composeHTMLStringWithCampaign:(AIDCampaign *)campaign BasedOnState:(AIDSessionState) state {

    NSString *content = @"";
    if (state != AIDSessionState_ShowOffer && state != AIDSessionState_ShowPresent) {
        NSLog(@"state should be either AIDSessionState_ShowOffer or AIDSessionState_ShowPresent");
        return content;
    }
    content = @"<!DOCTYPE html><html><title>HTML Tutorial</title><style>";
    content = [content stringByAppendingString:campaign.css];
    content = [content stringByAppendingString:@"</style>"];
    content = [content stringByAppendingString:@"<body><div id='zc-plugincontainer'>"];
    if (state == AIDSessionState_ShowOffer) {
        content = [content stringByAppendingString:campaign.offer];
    } else if (state == AIDSessionState_ShowPresent) {
        content = [content stringByAppendingString:campaign.present];
    }
    content = [content stringByAppendingString:@"</div></body></html>"];
    NSLog(@"%@", content);
    return content;
}

- (void)showCreativeBySessionState:(AIDSessionState)state ElementId:(NSNumber *)elementId
                        CouponCode:(NSString *)couponCode CompletionHandler:(void (^)(BOOL success))completionHandler {
    
    if (state == AIDSessionState_Undefined || state == AIDSessionState_Disable) {
        completionHandler(NO);
        return;
    }

    AIDCampaign *campaign = [[AIDConfigCampaignManager instance] getCampaignByElementId:elementId];
    if (campaign) {
    
        //Dismiss the existing one
        [self dismissCreative:^{
            
            AIDWebViewController *inAppWebViewCtl = nil;
            
            NSString *composedHTML = [self composeHTMLStringWithCampaign:campaign BasedOnState:state];
            inAppWebViewCtl = [[AIDWebViewController alloc] initWithHTMLString:composedHTML CouponCode:couponCode];
            if (inAppWebViewCtl) {
                inAppWebViewCtl.delegate = self;
                           
                //Create a transparent host view for presenting the web view
                UIWindow* mainWindow = [[UIApplication sharedApplication] keyWindow];
                topInAppWebViewPresenter = [[UIViewController alloc] init];
                [[topInAppWebViewPresenter view] setBackgroundColor:[UIColor clearColor]];
                [[topInAppWebViewPresenter view] setOpaque:FALSE];
                inAppWebViewCtl.modalPresentationStyle = UIModalPresentationFullScreen;
                [mainWindow addSubview:[topInAppWebViewPresenter view]];
                [topInAppWebViewPresenter presentViewController:inAppWebViewCtl animated:YES completion:nil];

                [self.delegate creativeHasPresented];
            }
        }];
        completionHandler(YES);
        return;
    }
    completionHandler(NO);
}

#pragma mark - [Delegate] AIDWebViewControllerDelegate
- (void)aidWebViewDismissWithPermanentClose:(BOOL)permanentClose WebView:(AIDWebViewController *)inAppWebView {
    
    [self dismissCreative:nil];
}

- (void)aidWebViewCopiedCoupon {
    
    [self.delegate creativeHasClickedCouponCode];
}
@end
