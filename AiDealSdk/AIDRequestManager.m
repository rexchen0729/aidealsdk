//
//  AIDRequestManager.m
//  AiDealSdk
//
//  Created by Appier on 2019/7/16.
//  Copyright © 2019 Appier. All rights reserved.
//

#import "AIDRequestManager.h"
#import "AIDUtility.h"
#import "AIDImageManager.h"
#import "NSData+AIDGZIP.h"
//#import "QGDeviceInfo.h"

//#define QG_ROLLBAR_URL @"https://api.rollbar.com/api/1/item/"

@implementation AIDRequestManager
/*
+ (void)requestInAppCampaigns:(void (^)(NSData * _Nullable, NSURLResponse * _Nonnull, NSError * _Nullable))completionHandler {

    NSString *appId = [[AIDUtility sharedManager] getUserDefaultsForKey:QG_APP_ID];
    NSNumber *userId = [[AIDUtility sharedManager] getUserDefaultsForKey:QG_USER_ID];
    if (!appId.isValidStr || ![AIDUtility isValidObject:userId]) {
        NSLog(@"%@ : %s : sdk not initialised", self, __func__);
        NSError *err = [NSError errorWithDomain:@"some_domain" code:0 userInfo:@{ NSLocalizedDescriptionKey:@"AppId or UserId is nil"}];
        completionHandler(nil, [NSURLResponse new], err);
        return;
    }
    long versionNo = 0;
    if ([[AIDUtility sharedManager] isUserDefaultsForKey:QG_IN_APP_VERSION_NO]) {
        versionNo = [[[AIDUtility sharedManager] getUserDefaultsForKey:QG_IN_APP_VERSION_NO] longValue];
        //
        //  subtract 1 hour to fetch delayed data from server
        //  this will fetch all data before 1 hour from the last fetch timestamp
        //
        if (versionNo > 3600) {
            versionNo -= 3600;
        }
    }
    
    //---download close button image---
    [[AIDImageManager instance] downloadImageForURL:QG_INAPP_CLOSE_IMAGE_URL];
    
    NSString *urlString = [NSString stringWithFormat:@"%@?appId=%@&userId=%@&versionNo=%ld&device=ios&inapp=1", QG_USERS_BACKEND_BASE_URL, appId, userId, versionNo];
    NSURL *url = [NSURL URLWithString:[urlString stringByRemovingPercentEncoding]];
    NSURLRequest *request = [NSURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:60];
    
    NSURLSessionDataTask *task = [[AIDImageManager session] dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        completionHandler(data, response, error);
    }];
    [task resume];
}

+(void)requestExitPushDataFromServer:(void (^)(NSData * _Nullable data, NSURLResponse *response, NSError * _Nullable error))completionHandler {
    
    if ([[[AIDUtility sharedManager] getUserDefaultsForKey:QG_IS_NOT_FIRST_APP_BACKGROUND] boolValue]) {
        return;
    }
    NSString *appId = [[AIDUtility sharedManager] getUserDefaultsForKey:QG_APP_ID];
    NSNumber *userId = [[AIDUtility sharedManager] getUserDefaultsForKey:QG_USER_ID];
    if (!appId.isValidStr || ![AIDUtility isValidObject:userId]) {
        NSLog(@"%@ : %s : sdk not initialised", self, __func__);
        return;
    }
    NSString *urlString = [NSString stringWithFormat:@"%@?appId=%@&userId=%@&device=ios&exit-push=1",QG_USERS_BACKEND_BASE_URL, appId, userId];
    
    NSURL *url = [NSURL URLWithString:[urlString stringByRemovingPercentEncoding]];
    NSURLRequest *request = [NSURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:60];
    NSURLSessionConfiguration *conf = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:conf];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        completionHandler(data, response, error);
    }];
    [task resume];
}

+(void)requestPersonalizationConfigFromServer:(void (^)(NSData * _Nullable data, NSURLResponse *response, NSError * _Nullable error))completionHandler {
    
    NSString *appId = [[AIDUtility sharedManager] getUserDefaultsForKey:QG_APP_ID];
    NSNumber *userId = [[AIDUtility sharedManager] getUserDefaultsForKey:QG_USER_ID];

    if (!appId.isValidStr || ![QGUtility isValidObject:userId]) {
        NSLog(@"%@ : %s : sdk not initialised", self, __func__);
        return;
    }
    NSString *urlStr = [AIQP_PERSONALIZE_URL stringByAppendingFormat:@"?os=ios&verNo=%@&appId=%@&userId=%@&appVerName=%@",
                            QGAppVersion(),
                            appId,
                            userId,
                            QGAppBuildNumber()];
    
    NSURL *url = [NSURL URLWithString:[urlStr stringByRemovingPercentEncoding]];
    NSURLRequest *request = [NSURLRequest requestWithURL:url
                                             cachePolicy:NSURLRequestUseProtocolCachePolicy
                                         timeoutInterval:60];
    NSURLSessionConfiguration *conf = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:conf];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                            completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        completionHandler(data, response, error);
    }];
    [task resume];
}

+ (voId)requestShowPushPrompt:(void (^)(NSData * _Nullable, NSURLResponse * _Nonnull, NSError * _Nullable))completionHandler {
    
    QGUtility *util = [QGUtility sharedManager];
    if ([util isUserDefaultsForKey:QG_SHOW_PUSH_PROMPT]) {
        if([util getUserDefaultsForKey:QG_SHOW_PUSH_PROMPT]) {
            return;
        }
    }
    NSString *appId = [util getUserDefaultsForKey:QG_APP_ID];
    NSNumber *userId = [util getUserDefaultsForKey:QG_USER_ID];
    if (!appId.isValidStr || ![QGUtility isValidObject:userId]) {
        NSLog(@"%@ : %s : sdk not initialised", self, __func__);
        return;
    }
    NSString *urlString = [NSString stringWithFormat:@"%@?appId=%@&userId=%@&device=ios&push-prompt=1",QG_USERS_BACKEND_BASE_URL, appId, userId];
    
    NSURL *url = [NSURL URLWithString:[urlString stringByRemovingPercentEncoding]];
    NSURLRequest *request = [NSURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:60];
    NSURLSessionConfiguration *conf = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:conf];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        completionHandler(data, response, error);
    }];
    [task resume];
}

+ (void)uploadCrashLog:(NSData*) postData CompletionHandler:(void (^)(NSURLResponse *response, NSError * _Nullable error))completionHandler {
    NSURLRequest *request = [QGRequestManager rollbarApiRequestWithBody:postData withGzippedData:false];
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse * _Nullable response, NSData * _Nullable data, NSError * _Nullable connectionError) {
        completionHandler(response, connectionError);
    }];
}

+ (NSURLRequest *)rollbarApiRequestWithBody:(NSData *)body withGzippedData:(BOOL)gzipped
{
    NSURL *URL = [NSURL URLWithString:QG_ROLLBAR_URL];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:URL];
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody: body];
    [request setTimeoutInterval:10];
    return request;
}
*/
@end
