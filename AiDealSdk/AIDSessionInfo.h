//
//  AIDSessionInfo.h
//
//  Created by Appier on 2020/2/28.
//  Copyright © 2020 Appier. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AIDSessionState.h"

NS_ASSUME_NONNULL_BEGIN

@interface AIDReceiveCampaignEvent : NSObject <NSCoding>
@property(nonatomic, copy) NSNumber *campaign_id;
@property(nonatomic, copy) NSNumber *element_id;
@property(nonatomic, copy) NSString *cron_time;
@property(nonatomic, copy) NSString *policy_id;
@property(nonatomic, copy) NSString *name;
@property(nonatomic, copy) NSString *coupon_code;
@property(nonatomic, copy) NSString *incentive_id;
@property(nonatomic, copy) NSString *incentive_type;

- (instancetype)initWithDictinary:(NSDictionary *)campaignInfo;
@end

@interface AIDPageInfo : NSObject <NSCoding>
@property(nonatomic, copy) NSString *uuid;
@property(nonatomic, copy) NSString *usid;
@property(nonatomic, copy) NSString *sid;
@property(nonatomic, copy) NSString *device;
@property(nonatomic, assign) BOOL behaviorSuppressed;
@property(nonatomic, copy) NSNumber *itemPrice;
@property(nonatomic, copy) NSNumber *cartPrice;

- (instancetype)initWithDictinary:(NSDictionary *)pageInfo;
@end

@interface AIDSessionUserState : NSObject <NSCoding>
@property(nonatomic, assign) BOOL showedPresentView;
@property(nonatomic, assign) BOOL showedOfferView;
@property(nonatomic, assign) BOOL countDownEnded;
@property(nonatomic, assign) BOOL visitedCartPage;
@property(nonatomic, assign) BOOL visitedCartFormPageBeforeEvent;
@property(nonatomic, assign) NSInteger sessionState;
@end

@interface AIDSessionInfo : NSObject <NSCoding>
@property(nonatomic, strong) AIDPageInfo *pageInfo;
@property(nonatomic, strong) AIDSessionUserState *userState;
@property(nullable, nonatomic, strong) AIDReceiveCampaignEvent *receivedCampaign;
@property(nonatomic, copy) NSString *sessionInActivityStartTimestamp;
@end

NS_ASSUME_NONNULL_END
