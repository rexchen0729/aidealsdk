//
//  AIDImageManager.m
//  QGSdk
//
//  Created by Appier
//  Copyright (c) 2019 APPIER INC. All rights reserved.
//

#import "AIDImageManager.h"
#import "AIDUtility.h"
#import <CommonCrypto/CommonDigest.h>

static const int RetryCount = 2;
@class AIDImageManager;

@interface AIDDownloadTaskList : NSObject
{
    NSMutableDictionary *taskRetryDict;
}
- (void)addTaskUrl:(NSString *)urlString;
@end

@implementation AIDDownloadTaskList
- (instancetype)init {
    self = [super init];
    return self;
}

- (void)retryIfPossible:(NSString *)urlString {
    if ([self->taskRetryDict objectForKey:urlString]) {
        int count = [[self->taskRetryDict objectForKey:urlString] intValue];
        if (--count >= 0) {
            NSLog(@"start retrying and retry count:%d left", count);
            [self requestImp:urlString];
        }
        if (count < 0) {
            NSLog(@"Remove from lookup tbl----------------");
            [self->taskRetryDict removeObjectForKey:urlString];
        } else {
            [self->taskRetryDict setValue:[NSNumber numberWithInt:count] forKey:urlString];
        }
    } else {
        NSLog(@"No retry count");
    }
}

- (void)requestImp:(NSString *)urlString {
    NSLog(@"Try download %@", urlString);
    NSURL *url = [NSURL URLWithString:urlString];
    NSURLSessionDownloadTask *task = [[AIDImageManager session] downloadTaskWithURL:url completionHandler:^(NSURL *location, NSURLResponse *response, NSError *error) {
        if (error) {
            NSLog(@"downloadTaskWithRequest failed: %@", error);
            [self retryIfPossible:urlString];
            return;
        }
        UIImage *image = [UIImage imageWithData:[NSData dataWithContentsOfURL:location]];
        if (![AIDUtility isValidObject:image]) {
            //invalid image
            [self retryIfPossible:urlString];
            return;
        }
        [[AIDImageManager instance] saveImageToFileManager:[NSData dataWithContentsOfURL:location] ForURL:urlString];
    }];
    
    [task resume];
}

- (void)addTaskUrl:(NSString *)urlString {
    if (taskRetryDict == nil) {
        taskRetryDict = [NSMutableDictionary new];
    }
    //If the incoming url is not in the lookup table, we put it in the table
    if ([taskRetryDict objectForKey:urlString] == nil) {
        [taskRetryDict setValue:[NSNumber numberWithInt:RetryCount] forKey:urlString];
    }
    [self requestImp:urlString];
}

@end

@interface AIDImageManager ()
{
    AIDDownloadTaskList *dwnTaskList;
}
@end

@implementation AIDImageManager

+ (instancetype)instance {
    static AIDImageManager *instance_ = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance_ = [[self alloc] init];
    });
    return instance_;
}

+ (NSURLSession *)session {
    static NSURLSession *session = nil;
    static dispatch_once_t once_token;
    dispatch_once(&once_token, ^{
        NSURLSessionConfiguration *conf = [NSURLSessionConfiguration defaultSessionConfiguration];
        session = [NSURLSession sessionWithConfiguration:conf];
    });
    return session;
}

//----downloads an image and saves to file manager if no error----
- (void)downloadImageForURL:(NSString *)urlString {
    if (!urlString.isValidStr) return;
    
    if ([self isImageAvailableForURL:urlString]) return;
    
    if (dwnTaskList == nil) {
        dwnTaskList = [AIDDownloadTaskList new];
    }
    [dwnTaskList addTaskUrl:urlString];
}

- (UIImage *)getImageForURL:(NSString *)urlString {
    if (!urlString.isValidStr) {
        return nil;
    }
    NSData *data = [self getImageFromFileManager:urlString];
    if (data) {
        return [UIImage imageWithData:data];
    }
    return nil;
}

- (void)deleteImageForURL:(NSString *)urlString {
    if (!urlString.isValidStr)
        return;
    
    [self deleteImageFromFileManager:urlString];
}

#pragma mark - File Manager for images

//----save a file in the file manager----
- (void)saveImageToFileManager:(NSData *)data ForURL:(NSString *)url {
    NSError *error = nil;
    if (data && url.isValidStr) {
        NSString *filePath = [self getTrimmedFilenameForImageURL:url];
        [data writeToFile:filePath options:NSDataWritingAtomic error:&error];
    }
    
    if (error) {
        //NSLog(@"Error Saving File: %@",error.description);
    }
}

//---return an image (NSData) from file manager----
- (NSData *)getImageFromFileManager:(NSString *)url {
    if ([self isImageAvailableForURL:url]) {
        NSString *path = [self getTrimmedFilenameForImageURL:url];
        NSData *data = [[NSFileManager defaultManager] contentsAtPath:path];
        return data;
    }else {
        return nil;
    }
}

//---delete a particular image from file manager----
- (void)deleteImageFromFileManager:(NSString *)urlString {
    if ([self isImageAvailableForURL:urlString]) {
        NSString *path = [self getTrimmedFilenameForImageURL:urlString];
        NSError *error = nil;
        [[NSFileManager defaultManager] removeItemAtPath:path error:&error];
        if (error) {
            //NSLog(@"error deleting file %@", error.description);
        }
    }
}

//----delete all images downloaded by the sdk----
- (void)deleteAllImagesFromFileManager {
    NSString *directory = [self getDocumentsDirectory];
    NSError *error = nil;
    [[NSFileManager defaultManager] removeItemAtPath:directory error:&error];
    if (error) {
        //NSLog(@"Error Deleting Directory: %@", error.description);
    }
}

//---checks if an image is available in the file manager---
- (BOOL)isImageAvailableForURL:(NSString *)urlString {
    BOOL isImage = NO;
    NSString *path = [self getTrimmedFilenameForImageURL:urlString];
    if ([[NSFileManager defaultManager] fileExistsAtPath:path]) {
        isImage = YES;
    }
    return isImage;
}

//-----returns the base path of the file manager
- (NSString *)getDocumentsDirectory {
    NSArray * paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString * basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
    basePath = [basePath stringByAppendingPathComponent:@"Qgraph_sdk"];
    
    if ([[NSFileManager defaultManager] fileExistsAtPath:basePath] == NO) {
        [[NSFileManager defaultManager] createDirectoryAtPath:basePath withIntermediateDirectories:YES attributes:nil error:nil];
    }
    
    return basePath;
}

//----return the path of the image in the file manager
- (NSString *)getTrimmedFilenameForImageURL:(NSString *)imageURL {
    NSString *basePath = [self getDocumentsDirectory];
//    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"[^a-zA-Z0-9_]+" options:0 error:nil];
//    NSString *imagePath = [regex stringByReplacingMatchesInString:imageURL options:0 range:NSMakeRange(0, imageURL.length) withTemplate:@""];
 
    NSString *imagePath = [self _cacheKeyForImageURL:imageURL];
    NSString *path = [basePath stringByAppendingPathComponent:imagePath];
    
    return path;
}

- (NSString *)_cacheKeyForImageURL:(NSString *)urlString {
    return AIDFBMD5HashFromString(urlString);
}

NSString *AIDFBMD5HashFromData(NSData *data) {
    unsigned char md[CC_MD5_DIGEST_LENGTH];

    // NOTE: `__block` variables of a struct type seem to be bugged. The compiler emits instructions to read
    // from the stack past where they're supposed to exist. This fixes that, by only using a traditional pointer.
    CC_MD5_CTX ctx_val = { 0 };
    CC_MD5_CTX *ctx_ptr = &ctx_val;
    CC_MD5_Init(ctx_ptr);
    [data enumerateByteRangesUsingBlock:^(const void *bytes, NSRange byteRange, BOOL *stop) {
        CC_MD5_Update(ctx_ptr , bytes, (CC_LONG)byteRange.length);
    }];
    CC_MD5_Final(md, ctx_ptr);

    NSString *string = [NSString stringWithFormat:@"%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x",
                        md[0], md[1],
                        md[2], md[3],
                        md[4], md[5],
                        md[6], md[7],
                        md[8], md[9],
                        md[10], md[11],
                        md[12], md[13],
                        md[14], md[15]];
    return string;
}

NSString *AIDFBMD5HashFromString(NSString *string) {
    return AIDFBMD5HashFromData([string dataUsingEncoding:NSUTF8StringEncoding]);
}

@end
