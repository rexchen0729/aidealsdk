//
//  AIDSocketController.m
//
//  Created by Appier on 2020/2/7.
//  Copyright © 2020 Appier. All rights reserved.
//

#import "AIDSocketController.h"
#import "AIDSIOSocket.h"
#define USE_LOCAL 0

#if NEW_STAGING
#define AID_SOCKET_HOST @"http://10.10.71.157/%@"
#else
#define AID_SOCKET_HOST @"https://visitor-fleet.zenclerk.com/%@"
#endif

@interface AIDSocketController()
@property(nonatomic, strong) AIDSIOSocket* sioSocket;
@property(nonatomic, copy) NSString* uuid;
@property(nonatomic, copy) NSString* usid;
@property(nonatomic, copy) NSString* sid;
@property(nonatomic, assign) BOOL bConnected;
@end
@implementation AIDSocketController
@synthesize sioSocket;
@synthesize uuid;
@synthesize usid;
@synthesize sid;
@synthesize bConnected;

- (void)connectWithApiKey:(NSString *)apiKey {
    
    if (sioSocket) {
        [sioSocket close];
        sioSocket = nil;
    }
    __weak AIDSocketController* wself = self;
    NSString *host = [NSString stringWithFormat:AID_SOCKET_HOST, apiKey];
    NSLog(@"Socket Host:%@", host);
    [AIDSIOSocket socketWithHost:host response: ^(AIDSIOSocket *socket) {
        sioSocket = socket;
        sioSocket.onConnect = ^() {
            NSLog(@"connect");
            bConnected = YES;
            [wself.delegate onAIDSocketConnected];
        };
        
        sioSocket.onDisconnect = ^() {
            bConnected = NO;
            [wself.delegate onAIDSocketDisconnected];
        };
        
        sioSocket.onReconnect = ^(NSInteger numberOfAttempts) {
            NSLog(@"reconnect:%ld", (long)numberOfAttempts);
        };

        [sioSocket on: @"campaign_event" callback:^(SIOParameterArray *args) {
        
        }];
        
        [sioSocket on: @"ReceiveCampaign" callback:^(SIOParameterArray *args) {
            NSLog(@"%@", args.firstObject);
            [wself.delegate onAIDSocketReceivedCampaign:args.firstObject];
        }];
        
        sioSocket.onReconnect = ^(NSInteger numberOfAttempts) {
            NSLog(@"onReconnect:%ld", (long)numberOfAttempts);
        };
        
        [sioSocket on: @"init" callback:^(SIOParameterArray *args) {
            [wself.delegate onAIDSocketInitPageCompleted:args.firstObject];
        }];
    }];
}

- (void)disconnect {
    
    if (sioSocket) {
        [sioSocket close];
        sioSocket = nil;
    }
    uuid = nil;
    usid = nil;
    sid = nil;
    bConnected = NO;
}

- (BOOL)initWithPageParams:(NSDictionary *)params {
    
    if (params && sioSocket && bConnected) {
        SIOParameterArray *arglist = [SIOParameterArray arrayWithObject:params];
        NSLog(@"[init par]==>%@", arglist);
        __weak AIDSocketController* wself = self;
        [sioSocket emit:@"init" args:arglist ack:^(SIOParameterArray * args) {
            [wself.delegate onAIDSocketInitPageCompleted:args.firstObject];
        }];
        return YES;
    }
    return NO;
}

- (BOOL)isConnected {
    
    return bConnected;
}

- (void)sendEventWithName:(NSString *)name Parms:(id)parms {
    
    if ([self isConnected] && parms &&
        ([parms isKindOfClass:[NSDictionary class]] || [parms isKindOfClass:[NSArray class]]) && name.length) {
        SIOParameterArray *arglist = [SIOParameterArray arrayWithObject:parms];
        [sioSocket emit:name args:arglist];
    }
}
@end
