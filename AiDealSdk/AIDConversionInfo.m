//
//  AIDConversionInfo.m
//  AiDealSdk
//
//  Created by Appier on 2020/3/26.
//  Copyright © 2020 Appier. All rights reserved.
//

#import "AIDConversionInfo.h"
@implementation AIDConversionItem
@end

@implementation AIDConversionInfo
+(nullable NSDictionary *)convertInfoToDictionary:(AIDConversionInfo *)info {
    
    if (info) {
        NSMutableDictionary *dic = [NSMutableDictionary new];
        
        //If no conversionID, dont handle anything.
        if (info.conversionId == nil) {
            return nil;
        }
        [dic setObject:info.conversionId forKey:@"id"];
        if (info.conversionName) {
            [dic setObject:info.conversionName forKey:@"name"];
        }
        if (info.couponCodes) {
            [dic setObject:info.couponCodes forKey:@"coupon_codes"];
        }
        if (info.totalItems) {
            [dic setObject:info.totalItems forKey:@"item_count"];
        }
        if (info.totalPrice) {
            [dic setObject:info.totalPrice forKey:@"price"];
        }
        if (info.items && info.items.count) {
            NSMutableArray *items = [NSMutableArray new];
            for (int i = 0; i < info.items.count; i++) {
                id obj = [info.items objectAtIndex:i];
                if ([obj isKindOfClass:[AIDConversionItem class]]) {
                    NSMutableDictionary *oneDicItem = [NSMutableDictionary new];
                    AIDConversionItem *oneItem = (AIDConversionItem *)obj;
                    if (oneItem.identifier) {
                        [oneDicItem setValue:oneItem.identifier forKey:@"id"];
                    }
                    if (oneItem.count) {
                        [oneDicItem setValue:oneItem.count forKey:@"count"];
                    }
                    if (oneItem.name) {
                        [oneDicItem setValue:oneItem.name forKey:@"name"];
                    }
                    if (oneItem.url) {
                        [oneDicItem setValue:oneItem.url forKey:@"url"];
                    }
                    if (oneItem.price) {
                        [oneDicItem setValue:oneItem.price forKey:@"price"];
                    }
                    [items addObject:oneDicItem];
                }
            }
            [dic setObject:items forKey:@"items"];
        }
        return dic;
    }
    return nil;
}
@end
