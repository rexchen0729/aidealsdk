//
//  AIDWebViewController.m
//
//  Created by Appier on 19/2/19.
//  Copyright © 2019 Appier. All rights reserved.
//

#import "AIDWebViewController.h"
#import <WebKit/WebKit.h>
#import "AIDUtility.h"

@interface AIDWebViewController ()<WKScriptMessageHandler, WKNavigationDelegate>
{
    
}
@property(nonatomic, strong) UIButton *closeBtn;
@property(nonatomic, strong) WKWebView *webView;
@property(nonatomic, strong) UIView *tossView;
@property(nonatomic, strong) UILabel *tossText;
@property(nonatomic, strong) UIActivityIndicatorView *indicatorView;
@property(nonatomic, weak) NSLayoutConstraint *wkWidthConstraint;
@property(nonatomic, weak) NSLayoutConstraint *wkHeightConstraint;
@property(nonatomic, assign) float maxWidth;
@property(nonatomic, assign) float maxHeight;
@property(nonatomic, copy) NSString *htmlString;
@property(nonatomic, copy) NSString *couponCodeString;
@property(nonatomic, strong) UILabel *debugString;
@end

static NSString *QGSCRIPT = @"var aidMobileSdk={sendData:function(d){try{ var s={data:d,command:'data'};if(window.webkit && window.webkit.messageHandlers){ window.webkit.messageHandlers.aideal.postMessage(s)}}catch(err){}}}";
static NSString *FIT_VIEW_SCALE = @"var meta = document.createElement('meta'); meta.setAttribute('name', 'viewport'); meta.setAttribute('content', 'width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no'); document.getElementsByTagName('head')[0].appendChild(meta);";
static NSString *ATTRIBUTE_PARSER = @"document.addEventListener('click',function(e){e.preventDefault();var t=e.target||e.srcElement,a={};if(t.hasAttribute('aid-deeplink')){var i=t.getAttribute('aid-deeplink');i&&(a.deeplink=i)}a.copycode=t.hasAttribute('aid-copycode');a.close=t.hasAttribute('aid-close');Object.keys(a).length&&aidMobileSdk.sendData(a)},false);";
static NSString *DISABLE_SELECTION_CSS = @"<style type=\"text/css\">* {-webkit-touch-callout: none; -webkit-user-select: none;}</style>";

// Square W / H
static const CGFloat kButton_Size = 30.0;

@implementation AIDWebViewController

- (instancetype)initWithHTMLString:(NSString *)htmlString CouponCode:(NSString *)couponCode {
    self = [super init];
    self.htmlString = htmlString;
    self.couponCodeString = couponCode;
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = kAIDColorBackgroundOverlay;
    self.view.opaque = NO;
    
    // Tap gesture to dismiss
    UITapGestureRecognizer *tapGes = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onOverlayTap:)];
    [self.view addGestureRecognizer:tapGes];
    
    // Define max height
    CGSize scnsize = [UIScreen mainScreen].bounds.size;
    self.maxWidth = scnsize.width * 0.9;
    self.maxHeight = scnsize.height * 0.9;
    
    //---notifier when screen rotates---
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didRotate:) name:UIApplicationDidChangeStatusBarOrientationNotification object:nil];
    
    // 1.WKWebview
    // Script injection
    WKUserScript *aiqUserScript = [[WKUserScript alloc] initWithSource:QGSCRIPT
                                                      injectionTime:WKUserScriptInjectionTimeAtDocumentStart
                                                   forMainFrameOnly:NO];
    
    WKUserScript *scaleWebViewScript = [[WKUserScript alloc] initWithSource:FIT_VIEW_SCALE
                                                              injectionTime:WKUserScriptInjectionTimeAtDocumentEnd
                                                           forMainFrameOnly:YES];
    WKUserScript *attributeParseWebViewScript = [[WKUserScript alloc] initWithSource:ATTRIBUTE_PARSER
                                                                       injectionTime:WKUserScriptInjectionTimeAtDocumentEnd
                                                                    forMainFrameOnly:YES];
    self.webView = [[WKWebView alloc] initWithFrame:CGRectZero];
    [self.webView.configuration.userContentController addUserScript:aiqUserScript];
    [self.webView.configuration.userContentController addUserScript:scaleWebViewScript];
    [self.webView.configuration.userContentController addUserScript:attributeParseWebViewScript];
    [self.webView.configuration.websiteDataStore removeDataOfTypes:[WKWebsiteDataStore allWebsiteDataTypes]
                                                     modifiedSince:[[NSDate date] dateByAddingTimeInterval:-86400.0] completionHandler:^{}];
    [self.webView.configuration.userContentController addScriptMessageHandler:self name:@"aideal"];
    
    // Discard invalid html string
    if ([AIDUtility isValidObject:self.htmlString]) {
        [self.webView loadHTMLString:self.htmlString baseURL:nil];
    }
    
    self.webView.translatesAutoresizingMaskIntoConstraints = false;
    self.webView.navigationDelegate = self;
    self.webView.alpha = 0;
    [self.view addSubview:self.webView];
    
    // Layout setup
    self.wkWidthConstraint = [NSLayoutConstraint constraintWithItem:self.webView attribute:NSLayoutAttributeWidth
    relatedBy:NSLayoutRelationEqual
       toItem:nil attribute:NSLayoutAttributeHeight multiplier:1.0 constant:100.0];
    self.wkHeightConstraint = [NSLayoutConstraint constraintWithItem:self.webView attribute:NSLayoutAttributeHeight
                                                      relatedBy:NSLayoutRelationEqual
                                                         toItem:nil attribute:NSLayoutAttributeHeight multiplier:1.0 constant:1.0];
    NSLayoutConstraint *centerX = [NSLayoutConstraint constraintWithItem:self.webView attribute:NSLayoutAttributeCenterX
                                                              relatedBy:NSLayoutRelationEqual
                                                                 toItem:self.view attribute:NSLayoutAttributeCenterX multiplier:1.0 constant:0.0];
    NSLayoutConstraint *centerY = [NSLayoutConstraint constraintWithItem:self.webView attribute:NSLayoutAttributeCenterY
                                                              relatedBy:NSLayoutRelationEqual
                                                                 toItem:self.view attribute:NSLayoutAttributeCenterY multiplier:1.0 constant:0.0];
    [self.webView addConstraint:self.wkWidthConstraint];
    [self.webView addConstraint:self.wkHeightConstraint];
    [self.view addConstraint:centerX];
    [self.view addConstraint:centerY];
    
    
    // 2.Close button
    // Layout setup
    self.closeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.view addSubview:self.closeBtn];
    self.closeBtn.alpha = 0;
    self.closeBtn.translatesAutoresizingMaskIntoConstraints = NO;
    
    NSLayoutConstraint *wBtnConstrait = [NSLayoutConstraint constraintWithItem:self.closeBtn attribute:NSLayoutAttributeWidth
                                                           relatedBy:NSLayoutRelationEqual
                                                              toItem:nil attribute:NSLayoutAttributeWidth multiplier:1.0 constant:kButton_Size];
    NSLayoutConstraint *hBtnConstrait = [NSLayoutConstraint constraintWithItem:self.closeBtn attribute:NSLayoutAttributeHeight
                                                           relatedBy:NSLayoutRelationEqual
                                                              toItem:nil attribute:NSLayoutAttributeHeight multiplier:1.0 constant:kButton_Size];
    [self.closeBtn addConstraint:wBtnConstrait];
    [self.closeBtn addConstraint:hBtnConstrait];
    
    NSLayoutConstraint *closeBtnTopMarginConstraint = [NSLayoutConstraint constraintWithItem:self.closeBtn attribute:NSLayoutAttributeTop
                                                                                     relatedBy:NSLayoutRelationEqual
                                                                                        toItem:self.webView attribute:NSLayoutAttributeTop multiplier:1.0 constant:-kButton_Size/2];
    NSLayoutConstraint *closeBtnRightMarginConstraint = [NSLayoutConstraint constraintWithItem:self.closeBtn attribute:NSLayoutAttributeTrailing
                                                                relatedBy:NSLayoutRelationEqual
                                                                   toItem:self.webView attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:kButton_Size/2];
    [self.view addConstraint:closeBtnTopMarginConstraint];
    [self.view addConstraint:closeBtnRightMarginConstraint];
    
    // Event
    [self.closeBtn addTarget:self action:@selector(dismiss) forControlEvents:UIControlEventTouchDown];
    
    // 3. Indicator View
    self.indicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    self.indicatorView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:self.indicatorView];
    self.indicatorView.hidesWhenStopped = YES;
    [self.indicatorView startAnimating];
    NSLayoutConstraint *indicator_centerX = [NSLayoutConstraint constraintWithItem:self.indicatorView attribute:NSLayoutAttributeCenterX
                                                                         relatedBy:NSLayoutRelationEqual
                                                                            toItem:self.view attribute:NSLayoutAttributeCenterX multiplier:1.0 constant:0.0];
    [self.view addConstraint:indicator_centerX];
    NSLayoutConstraint *indicator_centerY = [NSLayoutConstraint constraintWithItem:self.indicatorView attribute:NSLayoutAttributeCenterY
                                                              relatedBy:NSLayoutRelationEqual
                                                                 toItem:self.view attribute:NSLayoutAttributeCenterY multiplier:1.0 constant:0.0];
    [self.view addConstraint:indicator_centerY];
    
    // Draw Content
    // Background
    CAShapeLayer *circleLayer = [CAShapeLayer layer];
    [circleLayer setPath:[[UIBezierPath bezierPathWithOvalInRect:CGRectMake(0, 0, kButton_Size, kButton_Size)] CGPath]];
    [self.closeBtn.layer addSublayer:circleLayer];
    [circleLayer setFillColor:[UIColor  colorWithRed:0 green:0 blue:0 alpha:0.84].CGColor];
    
    float degree = 0;
    float radian = 0;
    float half = kButton_Size / 2 * 0.77;
    float x = 0;
    float y = 0;
    
    // (Cross shape - X)
    CAShapeLayer *lineLayer01 = [CAShapeLayer new];
    UIBezierPath *onelinepath01 = [UIBezierPath new];
    degree = -135.0f;
    radian = degree * M_PI / 180.0f;
    x = half * cos(radian);
    y = half * sin(radian);
    [onelinepath01 moveToPoint:CGPointMake(x + kButton_Size / 2, y + kButton_Size / 2)];
    
    degree = 45.0;
    radian = degree * M_PI / 180.0f;
    x = half * cos(radian);
    y = half * sin(radian);
    [onelinepath01 addLineToPoint:CGPointMake(x + kButton_Size / 2, y + kButton_Size / 2)];
    lineLayer01.strokeColor = [UIColor whiteColor].CGColor;
    lineLayer01.lineWidth = 1;
    lineLayer01.path = onelinepath01.CGPath;
    [self.closeBtn.layer addSublayer:lineLayer01];

    CAShapeLayer *lineLayer02 = [CAShapeLayer new];
    UIBezierPath *onelinepath02 = [UIBezierPath new];
    degree = -45.0f;
    radian = degree * M_PI / 180.0f;
    x = half * cos(radian);
    y = half * sin(radian);
    [onelinepath02 moveToPoint:CGPointMake(x + kButton_Size / 2, y + kButton_Size / 2)];
    
    degree = -225.0f;
    radian = degree * M_PI / 180.0f;
    x = half * cos(radian);
    y = half * sin(radian);
    [onelinepath02 addLineToPoint:CGPointMake(x + kButton_Size / 2, y + kButton_Size / 2)];
    lineLayer02.strokeColor = [UIColor whiteColor].CGColor;
    lineLayer02.lineWidth = 1;
    lineLayer02.path = onelinepath02.CGPath;
    [self.closeBtn.layer addSublayer:lineLayer02];
    
    // (Border)
    self.closeBtn.layer.borderWidth = 1;
    self.closeBtn.layer.cornerRadius = kButton_Size / 2;
    self.closeBtn.layer.borderColor = [UIColor whiteColor].CGColor;
    
    // KVO Content Size
    [self.webView.scrollView addObserver:self forKeyPath:@"contentSize" options:NSKeyValueObservingOptionNew context:nil];
    
    // Toss view
    self.tossView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 300, 100)];
    self.tossText = [UILabel new];
    self.tossText.text = @"";
    self.tossView.backgroundColor = [UIColor colorWithRed:1 green:1 blue:1 alpha:0.7];
    self.tossView.layer.cornerRadius = 10;
    self.tossView.alpha = 0;
    self.tossView.translatesAutoresizingMaskIntoConstraints = NO;
    self.tossText.translatesAutoresizingMaskIntoConstraints = NO;
    [self.tossView addSubview:self.tossText];
    NSLayoutConstraint *tosstext_centerY = [NSLayoutConstraint constraintWithItem:self.tossText attribute:NSLayoutAttributeCenterY
                                                              relatedBy:NSLayoutRelationEqual
                                                                 toItem:self.tossView attribute:NSLayoutAttributeCenterY multiplier:1.0 constant:0.0];
    [self.tossView addConstraint:tosstext_centerY];
    NSLayoutConstraint *tosstext_centerX = [NSLayoutConstraint constraintWithItem:self.tossText attribute:NSLayoutAttributeCenterX
                                                              relatedBy:NSLayoutRelationEqual
                                                                 toItem:self.tossView attribute:NSLayoutAttributeCenterX multiplier:1.0 constant:0.0];
    [self.tossView addConstraint:tosstext_centerX];

    [self.view addSubview:self.tossView];
    
    NSLayoutConstraint *tossview_width = [NSLayoutConstraint constraintWithItem:self.tossView attribute:NSLayoutAttributeWidth
    relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeWidth multiplier:0.66 constant:0.0];
    NSLayoutConstraint *tossview_height = [NSLayoutConstraint constraintWithItem:self.tossView attribute:NSLayoutAttributeHeight
       relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeHeight multiplier:0.05 constant:0.0];
    NSLayoutConstraint *tossview_botY = [NSLayoutConstraint constraintWithItem:self.tossView attribute:NSLayoutAttributeBottomMargin
    relatedBy:NSLayoutRelationEqual
       toItem:self.view attribute:NSLayoutAttributeBottom multiplier:1.0 constant:-90.0];
    NSLayoutConstraint *tossview_centerX = [NSLayoutConstraint constraintWithItem:self.tossView attribute:NSLayoutAttributeCenterX
    relatedBy:NSLayoutRelationEqual
       toItem:self.view attribute:NSLayoutAttributeCenterX multiplier:1.0 constant:0.0];
    [self.view addConstraint:tossview_width];
    [self.view addConstraint:tossview_height];
    [self.view addConstraint:tossview_botY];
    [self.view addConstraint:tossview_centerX];
    
#if DEBUG
    // [QGAccessibilityManager assignInAppCustomView:self.webView Cross:self.closeBtn];
#endif
}

///----------------------------------
#pragma mark - Screen Rotation
///----------------------------------
- (void)didRotate:(NSNotification *)notification {

    CGSize scnsize = [UIScreen mainScreen].bounds.size;
    self.maxWidth = scnsize.width * 0.9;
    self.maxHeight = scnsize.height * 0.9;
    [self.webView.scrollView setNeedsLayout];
}

#pragma mark - Tap to dismiss
- (void)onOverlayTap:(UIGestureRecognizer *)ges {
    [self dismiss];
}

#pragma mark - WKNavigationDelegate
- (void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation
{
    // Disable user selection
    [webView evaluateJavaScript:@"document.documentElement.style.webkitUserSelect='none';" completionHandler:nil];
    // Disable callout
    [webView evaluateJavaScript:@"document.documentElement.style.webkitTouchCallout='none';" completionHandler:nil];
}

#pragma mark  - KVO monitor contentHeight
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSKeyValueChangeKey,id> *)change context:(void *)context {
    if (self.webView.isLoading == NO) {
        // Make sure content is completely calculated by WKWebview, so we wait a little; otherwise,
        // we sometimes get an incorrect content height
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{

            //  ---- group ----
            dispatch_group_t group = dispatch_group_create();

            // -- inject coupon code --
            dispatch_group_enter(group);
            NSString *injectStr = [NSString stringWithFormat:@"function injectCouponCode() { \
                                              document.querySelector('.zc_code_insert_code').innerText = '%@'}; injectCouponCode();",
                                   self.couponCodeString ? self.couponCodeString : @""];
            [self.webView evaluateJavaScript:injectStr completionHandler:^(id obj, NSError * error) {
                if (error == nil) {
                    NSLog(@"No error when inject");
                }
                           
                // leave
                dispatch_group_leave(group);
            }];

            
            // -- adjust size --
            dispatch_group_enter(group);
            [self.webView evaluateJavaScript:@"function getSize(){h=document.body.scrollHeight;w=document.body.scrollWidth;return{h,w}};getSize();"
                           completionHandler:^(id obj, NSError *error) {
                if (error == nil) {
                         
                    [self.indicatorView stopAnimating];
                         
                    NSDictionary *dic = obj;
                    int curContentW = [[dic objectForKey:@"w"] intValue];
                    int curContentH = [[dic objectForKey:@"h"] intValue];
                    NSLog(@"%d w:%f", curContentW, self.webView.scrollView.contentSize.width);
                    NSLog(@"%d h:%f", curContentH, self.webView.scrollView.contentSize.height);
                         
                    // Size Over ?
                    bool bOverMaxW = curContentW > self.maxWidth;
                    bool bOverMaxH = curContentH > self.maxHeight;
                         
                    // Disable scrolling or not
                    self.webView.scrollView.showsHorizontalScrollIndicator = bOverMaxW;
                    self.webView.scrollView.showsVerticalScrollIndicator = bOverMaxH;
                                             
                    // Adjust content width
                    float width = bOverMaxW ? self.maxWidth : curContentW;
                    self.wkWidthConstraint.constant = width;
                         
                    // Adjust content height
                    float height = bOverMaxH ? self.maxHeight : curContentH;
                    self.wkHeightConstraint.constant = height;
                         
                    [UIView animateWithDuration:0.2 animations:^{
                        self.webView.alpha = 1;
                        self.closeBtn.alpha = 1;
                        [self.view layoutIfNeeded];
                    }];
                }
                
                // leave
                dispatch_group_leave(group);
            }];

            dispatch_group_notify(group, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
                dispatch_async(dispatch_get_main_queue(), ^{
                    bool showVertical = self.webView.scrollView.showsVerticalScrollIndicator;
                    bool showHorizontal = self.webView.scrollView.showsHorizontalScrollIndicator;
                    [self.webView.scrollView setScrollEnabled: (showVertical || showHorizontal)];
                });
            });
            // ------
        });
    }
}

#pragma mark - Dismiss
- (void)dismiss {
    [self dismissPermantClose:NO];
}

- (void)dismissPermantClose:(BOOL)permanentClose {
    [self.webView.scrollView removeObserver:self forKeyPath:@"contentSize"];
    [self.webView.configuration.userContentController removeScriptMessageHandlerForName:@"aideal"];
    [self.delegate aidWebViewDismissWithPermanentClose:permanentClose WebView:self];
}

#pragma mark - Message Handler
- (void)userContentController:(nonnull WKUserContentController *)userContentController didReceiveScriptMessage:(nonnull WKScriptMessage *)message {
    if ([message.name isEqualToString: @"aideal"]) {
        NSDictionary *body = message.body;
        if ([body[@"command"] isEqual:@"data"]) {
            NSDictionary *data = body[@"data"];
            BOOL needClose = NO;
            BOOL haveCouponCode = NO;
            if ([AIDUtility isValidObject:[data objectForKey:@"close"]]) {
                 needClose = [[data objectForKey:@"close"] boolValue];
            }
            if ([AIDUtility isValidObject:data] && [data isKindOfClass:[NSDictionary class]]) {
                if ([AIDUtility isValidObject:[data objectForKey:@"copycode"]]) {
                    haveCouponCode = [[data objectForKey:@"copycode"] boolValue];
                }
            }
            if (haveCouponCode) {
                [self.delegate aidWebViewCopiedCoupon];
                self.tossText.text = @"Coupon copied to clipboard";
                [UIView animateWithDuration:0.3 delay:0 options:UIViewAnimationOptionCurveEaseIn animations:^{
                    self.tossView.alpha = 1;
                } completion:^(BOOL finished) {
                    [UIView animateWithDuration:0.31 delay:0.5 options:UIViewAnimationOptionCurveEaseOut animations:^{
                        self.tossView.alpha = 0;
                     } completion:^(BOOL finished) {
                         if (needClose) {
                             [self.delegate aidWebViewDismissWithPermanentClose:NO WebView:self];
                         }
                     }];
                }];
            } else if (needClose) {
                [self.delegate aidWebViewDismissWithPermanentClose:NO WebView:self];
            }
        }
     } else {
         NSLog(@"received message %@", message.name);
     }
}
@end
