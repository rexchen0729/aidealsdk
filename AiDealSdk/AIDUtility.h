//
//  AIDUtility.h
//  AiDealSdk
//
//  Created by Appier
//  Copyright (c) 2019 APPIER INC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "NSString+AIDStringUtil.h"
/*
typedef NS_ENUM(NSUInteger, QGDATATYPE) {
    QGDATATYPE_PROFILE = 0,
    QGDATATYPE_EVENTS = 1,
    QGDATATYPE_USERDETAILS = 2,
};

#define QG_SDK_VERSION                          @"6.0.0"
#define QG_LAST_CLICKED_NOTIFICATION_ID         @"lastClkdNotId"
#define QG_LAST_CLICKED_NOTIFICATION_TIME       @"qgLastClkdNotTime"
#define QG_NOTIFICATION_ID                      @"notificationId"
#define QG_CAMPAIGN_ID                          @"qg"
#define QG_KEY                                  @"QG"
#define QG_NOTIF_ID                             @"nid"
#define QG_NOTIFICATION_SOURCE                  @"source"
#define QG_CLICK_ATTRIBUTION_WINDOW             @"qgClickAttributionWindow"
#define QG_VIEW_THROUGH_ATTRIBUTION_WINDOW      @"qgViewThroughAttributionWindow"
#define QG_APP_ID                               @"qgAppId"
#define QG_APP_SECRET                           @"qgAppSecret"
#define QG_USER_ID                              @"qgUserId"
#define QG_DEVICE_TOKEN                         @"qgDeviceToken"
#define QG_UNKNOWN_VALUE                        @"Unknown"
#define QG_LAST_LOCATION                        @"qgLastLocation"
#define QG_APP_LAUNCH                           @"app_launched"
#define QG_FIRST_APP_LAUNCH                     @"first_app_launched"
#define QG_APP_LAUNCH_DELAY                     0.50
#define QG_IN_APP_VERSION_NO                    @"qg_last_synced_in_app_versionNo"
#define QG_IN_APP                               @"qg_in_app"
#define QG_LAST_VIEW_THROUGH_NOTIFICATION_ID    @"qg_last_view_through_notification_id"
#define QG_LAST_VIEW_THROUGH_TIME               @"qg_last_view_through_time"
#define QG_IN_APP_DISABLED_STATUS               @"qg_in_app_enabled_status"
#define QG_PROFILE_INFO_LAST_SENT_TIME          @"qg_profile_info_last_sent_time"
#define QG_USER_DETAILS_LAST_SENT_TIME          @"qg_user_details_last_sent_time"
#define QG_DEV_PROFILE                          @"qg_dev_profile"
#define QG_AIQ_PUSH_ENABLED_STATUS              @"aiq_push_enabled"
#define QG_INBOX                                @"qg_inbox"
#define QG_INBOX_LIMIT                          @"qg_inbox_limit"
#define QG_CAROUSEL_ACTION_CATEGORY             @"QGCAROUSEL"
#define QG_CAROUSEL_ACTION_OPEN_APP             @"qg_rich_push_action_open_app"
#define QG_CAROUSEL_ACTION_NEXT                 @"qg_carousel_action_next"

#define QG_CRASH_LOG_KEY                        @"qg_crash_log"
#define QG_SAVED_PUSH_NOTIFICATION              @"qg_saved_push_notification"

#define AIQP_CONFIG                             @"AIQP_CONFIG"
#define AIQP_DISABLED_STATUS                    @"aiqp_disabled_status"

#define QG_SAFARI_TRACKING_DISABLED_STATUS      @"qg_safari_user_tracking_disabled_status"
#define QG_SAFARI_DATA_SENT_STATUS              @"qg_safari_data_sent_to_server_status"

#define QG_APP_GROUP_NAME_SDK                   @"qg_app_group_name_main_sdk"
#define QG_FORCE_TOUCH_CAPABILITY_STATUS        @"qg_force_touch_capability_status"
#define QG_RICH_PUSH_SUPPORTED_STATUS           @"qg_rich_push_notification_supported_status"
#define QG_EXT_RICH_PUSH_DEEPLINK               @"qg_rich_push_deeplink"

#define QG_IS_NOT_FIRST_APP_BACKGROUND          @"qg_is_not_first_time_app_background_event"
#define QG_EXIT_PUSH_PAYLOAD                    @"qg_exit_push_payload"

#define QG_SHOW_PUSH_PROMPT                     @"qg_show_push_prompt"


// Staging and Production URLs
#if STAGING
#define QG_USERS_BACKEND_BASE_URL               @"https://staging-users.quantumgraph.com/qg-data"
#define QG_BATCHED_URL                          @"https://staging-api.quantumgraph.com/qga/"
#define QG_RECOMMENDATION_URL                   @"https://dync-stg.c.appier.net/w/"
#define AIQP_PERSONALIZE_URL                    @"https://config.stage.bot.qgraph.io/api/v1.0/user_config"
#define AIQP_CONFIG_CONTEXT_REPORT_URL          @"https://config.stage.bot.qgraph.io/api/v1.0/user_config/info"
#define ENVIRONMENT                             @"staging"
#elif NEW_STAGING
#define QG_USERS_BACKEND_BASE_URL               @"https://users.stg.qgraph.io/qg-data"
#define QG_BATCHED_URL                          @"https://dback.stg.qgraph.io/qga/"
#define QG_RECOMMENDATION_URL                   @"https://dync-stg.c.appier.net/w/"
#define AIQP_PERSONALIZE_URL                    @"https://config.stg.qgraph.io/api/v1.0/user_config"
#define AIQP_CONFIG_CONTEXT_REPORT_URL          @"https://config.stg.qgraph.io/api/v1.0/user_config/info"
#define ENVIRONMENT                             @"staging"
#else
#define QG_USERS_BACKEND_BASE_URL               @"https://users.quantumgraph.com/qg-data"
#define QG_BATCHED_URL                          @"https://api.quantumgraph.com/qga/"
#define QG_RECOMMENDATION_URL                   @"https://aiqua-dync.c.appier.net/w/"
#define AIQP_PERSONALIZE_URL                    @"https://config.quantumgraph.com/api/v1.0/user_config"
#define AIQP_CONFIG_CONTEXT_REPORT_URL          @"https://config.quantumgraph.com/api/v1.0/user_config/info"
#define ENVIRONMENT                             @"production"
#endif


#define QG_UNIVERSAL_LINK_DOMAINS               @"qg_universal_link_domains"

#define QG_INAPP_CLOSE_IMAGE_URL                @"https://cdn.qgraph.io/img/qg_close_button.png"
*/
#define kAIDColor(r, g, b, a)          [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:a]
#define kAIDColorBackgroundOverlay     kAIDColor(51, 45, 45, 0.9)
//#define kAIDColorMessage               kAIDColor(250, 250, 250, 1)
//#define kAIDColorClear                 kAIDColor(0, 0, 0, 0)
/*
#define SYSTEM_VERSION_EQUAL_TO(v)                  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedSame)
#define SYSTEM_VERSION_GREATER_THAN(v)              ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedDescending)
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedDescending)

//Font Family Name
#define kQGFontFamilyHelveticaNeue                @"HelveticaNeue"
#define kQGFontFamilyHelveticaNeueBold            @"HelveticaNeue-Bold"
#define kQGFontFamilyHelveticaNeueLight           @"HelveticaNeue-Light"
#define kQGFontFamilyHelveticaNeueMedium          @"HelveticaNeue-Medium"

#define kQGFont(familyName, s)            [UIFont fontWithName:familyName size:s]
#define kQGFontHelveticaNeue(s)           kQGFont(kQGFontFamilyHelveticaNeue, s)
#define kQGFontHelveticaNeueBold(s)       kQGFont(kQGFontFamilyHelveticaNeueBold, s)
#define kQGFontHelveticaNeueLight(s)      kQGFont(kQGFontFamilyHelveticaNeueLight, s)
#define kQGFontHelveticaNeueMedium(s)     kQGFont(kQGFontFamilyHelveticaNeueMedium, s)
#define kQGFontInAppMessage               kQGFontHelveticaNeue(16)
*/
@interface AIDUtility : NSObject

+ (instancetype)sharedManager;
/*
- (void)setAppId:(NSString *)appId withGroupName:(NSString *)group setDevProfile:(BOOL)devProfile;
- (BOOL)shouldUserDetailsBeSent;
- (NSDictionary *)getUserDetails;
*/
+ (NSUserDefaults *)userDefaults;
- (void)saveUserDefaultsWithValue:(id)value Key:(NSString*)key;
- (void)saveUserDefaultsWithInteger:(NSInteger)value forKey:(NSString *)key;
- (id)getUserDefaultsForKey:(NSString *)key;
- (NSInteger)getUserDefaultIntegerForKey:(NSString *)key;
- (void)removeUserDefaultsForKey:(NSString *)key;
- (BOOL)isUserDefaultsForKey:(NSString *)key;

 - (void)saveEncodedData:(id)encodableObject forKey:(NSString *)key;
- (id)getDecodedDataForKey:(NSString *)key;
/*
- (NSInteger)daysBetweenDate:(NSDate *)fromDateTime andDate:(NSDate *)toDateTime;

- (NSInteger)getClickAttributionWindow;
- (BOOL)isClickThroughAttribution;
- (BOOL)isViewThroughAttribution;
- (void)setViewThroughForNotificationId:(NSNumber *)notificationId;
- (void)setClickThroughForNotificationId:(NSNumber *)notificationId;
*/
+ (BOOL)isValidObject:(id)object;
/*
+ (void)assertPropertyTypes:(NSDictionary *)properties;
+ (BOOL)inBackground;
+ (BOOL)isRichPushSupported;
+ (BOOL)isRunningTests;
+ (NSArray<NSString *> *)crashLogConcernKeywords;
- (void)fetchShowPushPrompt;

- (void)handleDeepLink:(NSURL *)url;

- (NSString *)getAppSecret:(NSString *)appId;
*/
/*!
 @abstract
 use .timeIntervalSince1970 to get utc timestamp
 
 <pre>AIDCurrentTime().timeIntervalSince1970</pre>
 */
extern NSDate * AIDCurrentTime(void);

@end
