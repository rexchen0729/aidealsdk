//
//  NSString+AIDStringUtil.m
//  AiDealSdk
//
//  Created by Appier on 01/06/16.
//  Copyright © 2019 APPIER INC. All rights reserved.
//

#import "NSString+AIDStringUtil.h"

@implementation NSString (AIDStringUtil)

- (BOOL)isValidStr {
    BOOL valid = YES;
    
    NSString* str;
    str = [self stringByTrimmingCharactersInSet:
           [NSCharacterSet whitespaceAndNewlineCharacterSet]];
    if ( (str == nil)
        || ([str length] == 0)
        || ([str isKindOfClass:[NSNull class]])
        || ([str isEqualToString:@""])) {
        
        valid = NO;
    }
    
    return valid;
}

- (NSURL *)encodedURL {
    return [NSURL URLWithString:[[self stringByRemovingPercentEncoding] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]]];
}

@end
